Mugenhwcfg
==========


Overview
--------

`mugenhwcfg` is a Python tool developed to support the [Muen Project][1].
It retrieves hardware information from a running Linux system and produces a
Muen hardware configuration file in XML format.


Requirements
------------

`mugenhwcfg` requires Python 3. It also requires `iasl` (part of the
acpica-tools package) and `cpuid` to be installed:

    $ sudo apt-get install acpica-tools cpuid

Optionally, `mugenhwcfg` also uses the Python Package [lxml][3] to format the
generated XML file. You can get it (if not yet installed) with either:

    $ sudo apt-get install python3-lxml

or

    $ sudo pip install lxml

Installing mugenhwcfg
---------------------

`mugenhwcfg` can be obtained from the repository with the command:

    $ git clone --recursive https://git.codelabs.ch/git/muen/mugenhwcfg.git

This clones all submodules required by the tool as well as the source files.


Running mugenhwcfg
------------------

After installation, `mugenhwcfg` can be run with the following commands:

    $ sudo modprobe msr
    $ cd mugenhwcfg
    $ sudo ./mugenhwcfg.py

Root user permissions are necessary to allow `mugenhwcfg` to examine system
data.


Output
------

After running the tool, the output XML file will be produced in the tool
directory and will be named **output.xml**.

Any errors encountered by the tool will (by default) prevent the output file
from being generated. The tool can attempt to generate an output file anyway
(for manual editing) using the `-f / --force` argument.


Updating
--------

As the tool relies on external files (such as the [pci.ids][4] repository), some
of these files might need to be updated to retrieve accurate information.
You can utilise the `-u / --update` argument to download and update these files
automatically.


Optional Arguments
------------------

- `-u / --update`             Update files used by the tool
- `-f / --force`              Attempt to generate the output file despite errors


More about mugenhwcfg
---------------------

### How it works

`mugenhwcfg` scans Linux system files (*/sys, /proc, /dev*) for processor,
memory and device information needed by the Muen kernel. It then fills up
**PyXB** Python objects with the information and creates an XML file.


### Use of PyXB Library

`mugenhwcfg` utilises the [PyXB package][5] to generate
a Python binding file from a hardware configuration schema file which should be
included with the tool distribution at */schema/hardware_config.xsd*.
This generation happens the first time the tool is run, at the location
*/generated/hardware_config.py*. This binding file is then used to create and
fill objects that are later converted to XML in the output.

The **PyXB** package is included as a submodule in the `mugenhwcfg` repository
at: */contrib/pyxb*


### Use of pci.ids

To decode device names, `mugenhwcfg` parses the **pci.ids** file in
*/data/pci.ids*. **pci.ids** is a repository of PCI identification numbers
maintained by the good people [here][4].


Running Tests
-------------

The `mugenhwcfg` tests require the Python package [pytest][6]:

    $ sudo apt-get install python3-pytest

or

    $ sudo pip install pytest

After installing the required dependencies, the tests can be run with:

    $ py.test-3

To produce a coverage report, execute the following commands:

    $ sudo apt-get install python3-pytest-cov
    $ py.test-3 --cov=src

Append the `--cov-report html` option if a html report instead of terminal
output is desired.


Additional information on generated hardware file
-------------------------------------------------

`mugenhwcfg` alters the following in the XML output hardware file to match the
requirements of the Muen Kernel:

##### Memory
  - Omit memory blocks that are reserved
  - Size of memoryBlocks are rounded down to the nearest multiple of a page
  - Sets "allocatable" to false for memoryBlocks at addresses < 1 MiB

##### Devices (PCI)
  - Omit PCI Bridges
  - Omit non PCI-Express devices behind bridges
  - Size of memoryBlocks are rounded up to match the size of a page (4KiB)

##### Devices (Serial)
  - Omits serial devices on ports other than COM ports


Contact
-------

You can drop an email to the Muen development team's mailing list at

	muen-dev@googlegroups.com

or contact the author (Chen Chin Jieh) directly at

	chinjieh@gmail.com


Acknowledgements
----------------

Big thanks to Adrian and Reto for their unending guidance and advice!


[1]: https://muen.sk/ "Muen website"
[2]: https://www.python.org/ "Python"
[3]: http://lxml.de/ "LXML"
[4]: https://pci-ids.ucw.cz/ "The pci.ids repository"
[5]: http://pyxb.sourceforge.net/ "PyXB"
[6]: https://docs.pytest.org/en/latest/ "pytest"
