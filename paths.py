#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

# File containing paths
# =====================
import os
CURRENTDIR = os.path.dirname(__file__)

# Location of tmp folder
TEMP = CURRENTDIR + "/tmp/"

# Location of system files / directories
CPUINFO = "/proc/cpuinfo"
MEMMAP = "/sys/firmware/memmap/"
DEVICES = "/sys/bus/pci/devices/"
MSR = ["/dev/cpu/0/msr", "/dev/msr0"]
DMAR = "/sys/firmware/acpi/tables/DMAR"
DSDT = "/sys/firmware/acpi/tables/DSDT"
FADT = "/sys/firmware/acpi/tables/FACP"
MADT = "/sys/firmware/acpi/tables/APIC"
MCFG = ["/sys/firmware/acpi/tables/MCFG", "/sys/firmware/acpi/tables/MCFG1"]
DEVMEM = "/dev/mem"
DMI = "/sys/devices/virtual/dmi/id/"
SERIALS = "/sys/bus/pnp/drivers/serial/"
IOMMUGRPS = "/sys/kernel/iommu_groups/"

# Location of pci.ids
PCIIDS = CURRENTDIR + "/data/pci.ids"

# Location of PyXB Library
PYXB = CURRENTDIR + "/contrib/pyxb/"
PYXB_GEN = PYXB + "scripts/pyxbgen"

# Location of schema files
SCHEMAPATH = CURRENTDIR + "/schema/hardware_config.xsd"
SCHEMA_BINDING_PATH = CURRENTDIR + "/generated/hardware_config.py"

# Location of output file
OUTPUT = CURRENTDIR + "/output.xml"
