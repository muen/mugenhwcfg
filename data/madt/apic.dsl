/*
 * Intel ACPI Component Architecture
 * AML Disassembler version 20140926-64 [Oct  1 2014]
 * Copyright (c) 2000 - 2014 Intel Corporation
 * 
 * Disassembly of apic.dat, Fri Feb 23 17:35:23 2018
 *
 * ACPI Data Table [APIC]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "APIC"    [Multiple APIC Description Table (MADT)]
[004h 0004   4]                 Table Length : 00000224
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : A1
[00Ah 0010   6]                       Oem ID : "FUJ   "
[010h 0016   8]                 Oem Table ID : "D3279-A1"
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "AMI "
[020h 0032   4]        Asl Compiler Revision : 00010013

[024h 0036   4]           Local Apic Address : FEE00000
[028h 0040   4]        Flags (decoded below) : 00000001
                         PC-AT Compatibility : 1

[02Ch 0044   1]                Subtable Type : 00 [Processor Local APIC]
[02Dh 0045   1]                       Length : 08
[02Eh 0046   1]                 Processor ID : 00
[02Fh 0047   1]                Local Apic ID : 00
[030h 0048   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[034h 0052   1]                Subtable Type : 00 [Processor Local APIC]
[035h 0053   1]                       Length : 08
[036h 0054   1]                 Processor ID : 02
[037h 0055   1]                Local Apic ID : 02
[038h 0056   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[03Ch 0060   1]                Subtable Type : 00 [Processor Local APIC]
[03Dh 0061   1]                       Length : 08
[03Eh 0062   1]                 Processor ID : 04
[03Fh 0063   1]                Local Apic ID : 04
[040h 0064   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[044h 0068   1]                Subtable Type : 00 [Processor Local APIC]
[045h 0069   1]                       Length : 08
[046h 0070   1]                 Processor ID : 06
[047h 0071   1]                Local Apic ID : 06
[048h 0072   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[04Ch 0076   1]                Subtable Type : 00 [Processor Local APIC]
[04Dh 0077   1]                       Length : 08
[04Eh 0078   1]                 Processor ID : 08
[04Fh 0079   1]                Local Apic ID : 08
[050h 0080   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[054h 0084   1]                Subtable Type : 00 [Processor Local APIC]
[055h 0085   1]                       Length : 08
[056h 0086   1]                 Processor ID : 0A
[057h 0087   1]                Local Apic ID : 0A
[058h 0088   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[05Ch 0092   1]                Subtable Type : 00 [Processor Local APIC]
[05Dh 0093   1]                       Length : 08
[05Eh 0094   1]                 Processor ID : 0C
[05Fh 0095   1]                Local Apic ID : 0C
[060h 0096   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[064h 0100   1]                Subtable Type : 00 [Processor Local APIC]
[065h 0101   1]                       Length : 08
[066h 0102   1]                 Processor ID : 0E
[067h 0103   1]                Local Apic ID : 0E
[068h 0104   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[06Ch 0108   1]                Subtable Type : 00 [Processor Local APIC]
[06Dh 0109   1]                       Length : 08
[06Eh 0110   1]                 Processor ID : 10
[06Fh 0111   1]                Local Apic ID : 10
[070h 0112   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[074h 0116   1]                Subtable Type : 00 [Processor Local APIC]
[075h 0117   1]                       Length : 08
[076h 0118   1]                 Processor ID : 12
[077h 0119   1]                Local Apic ID : 12
[078h 0120   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[07Ch 0124   1]                Subtable Type : 00 [Processor Local APIC]
[07Dh 0125   1]                       Length : 08
[07Eh 0126   1]                 Processor ID : 14
[07Fh 0127   1]                Local Apic ID : 14
[080h 0128   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[084h 0132   1]                Subtable Type : 00 [Processor Local APIC]
[085h 0133   1]                       Length : 08
[086h 0134   1]                 Processor ID : 16
[087h 0135   1]                Local Apic ID : 16
[088h 0136   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[08Ch 0140   1]                Subtable Type : 00 [Processor Local APIC]
[08Dh 0141   1]                       Length : 08
[08Eh 0142   1]                 Processor ID : 18
[08Fh 0143   1]                Local Apic ID : 18
[090h 0144   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[094h 0148   1]                Subtable Type : 00 [Processor Local APIC]
[095h 0149   1]                       Length : 08
[096h 0150   1]                 Processor ID : 1A
[097h 0151   1]                Local Apic ID : 1A
[098h 0152   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[09Ch 0156   1]                Subtable Type : 00 [Processor Local APIC]
[09Dh 0157   1]                       Length : 08
[09Eh 0158   1]                 Processor ID : 1C
[09Fh 0159   1]                Local Apic ID : 1C
[0A0h 0160   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0A4h 0164   1]                Subtable Type : 00 [Processor Local APIC]
[0A5h 0165   1]                       Length : 08
[0A6h 0166   1]                 Processor ID : 1E
[0A7h 0167   1]                Local Apic ID : 1E
[0A8h 0168   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0ACh 0172   1]                Subtable Type : 00 [Processor Local APIC]
[0ADh 0173   1]                       Length : 08
[0AEh 0174   1]                 Processor ID : 01
[0AFh 0175   1]                Local Apic ID : 01
[0B0h 0176   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0B4h 0180   1]                Subtable Type : 00 [Processor Local APIC]
[0B5h 0181   1]                       Length : 08
[0B6h 0182   1]                 Processor ID : 03
[0B7h 0183   1]                Local Apic ID : 03
[0B8h 0184   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0BCh 0188   1]                Subtable Type : 00 [Processor Local APIC]
[0BDh 0189   1]                       Length : 08
[0BEh 0190   1]                 Processor ID : 05
[0BFh 0191   1]                Local Apic ID : 05
[0C0h 0192   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0C4h 0196   1]                Subtable Type : 00 [Processor Local APIC]
[0C5h 0197   1]                       Length : 08
[0C6h 0198   1]                 Processor ID : 07
[0C7h 0199   1]                Local Apic ID : 07
[0C8h 0200   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0CCh 0204   1]                Subtable Type : 00 [Processor Local APIC]
[0CDh 0205   1]                       Length : 08
[0CEh 0206   1]                 Processor ID : 09
[0CFh 0207   1]                Local Apic ID : 09
[0D0h 0208   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0D4h 0212   1]                Subtable Type : 00 [Processor Local APIC]
[0D5h 0213   1]                       Length : 08
[0D6h 0214   1]                 Processor ID : 0B
[0D7h 0215   1]                Local Apic ID : 0B
[0D8h 0216   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0DCh 0220   1]                Subtable Type : 00 [Processor Local APIC]
[0DDh 0221   1]                       Length : 08
[0DEh 0222   1]                 Processor ID : 0D
[0DFh 0223   1]                Local Apic ID : 0D
[0E0h 0224   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0E4h 0228   1]                Subtable Type : 00 [Processor Local APIC]
[0E5h 0229   1]                       Length : 08
[0E6h 0230   1]                 Processor ID : 0F
[0E7h 0231   1]                Local Apic ID : 0F
[0E8h 0232   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0ECh 0236   1]                Subtable Type : 00 [Processor Local APIC]
[0EDh 0237   1]                       Length : 08
[0EEh 0238   1]                 Processor ID : 11
[0EFh 0239   1]                Local Apic ID : 11
[0F0h 0240   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0F4h 0244   1]                Subtable Type : 00 [Processor Local APIC]
[0F5h 0245   1]                       Length : 08
[0F6h 0246   1]                 Processor ID : 13
[0F7h 0247   1]                Local Apic ID : 13
[0F8h 0248   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[0FCh 0252   1]                Subtable Type : 00 [Processor Local APIC]
[0FDh 0253   1]                       Length : 08
[0FEh 0254   1]                 Processor ID : 15
[0FFh 0255   1]                Local Apic ID : 15
[100h 0256   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[104h 0260   1]                Subtable Type : 00 [Processor Local APIC]
[105h 0261   1]                       Length : 08
[106h 0262   1]                 Processor ID : 17
[107h 0263   1]                Local Apic ID : 17
[108h 0264   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[10Ch 0268   1]                Subtable Type : 00 [Processor Local APIC]
[10Dh 0269   1]                       Length : 08
[10Eh 0270   1]                 Processor ID : 19
[10Fh 0271   1]                Local Apic ID : 19
[110h 0272   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[114h 0276   1]                Subtable Type : 00 [Processor Local APIC]
[115h 0277   1]                       Length : 08
[116h 0278   1]                 Processor ID : 1B
[117h 0279   1]                Local Apic ID : 1B
[118h 0280   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[11Ch 0284   1]                Subtable Type : 00 [Processor Local APIC]
[11Dh 0285   1]                       Length : 08
[11Eh 0286   1]                 Processor ID : 1D
[11Fh 0287   1]                Local Apic ID : 1D
[120h 0288   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[124h 0292   1]                Subtable Type : 00 [Processor Local APIC]
[125h 0293   1]                       Length : 08
[126h 0294   1]                 Processor ID : 1F
[127h 0295   1]                Local Apic ID : 1F
[128h 0296   4]        Flags (decoded below) : 00000001
                           Processor Enabled : 1

[12Ch 0300   1]                Subtable Type : 04 [Local APIC NMI]
[12Dh 0301   1]                       Length : 06
[12Eh 0302   1]                 Processor ID : 00
[12Fh 0303   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[131h 0305   1]         Interrupt Input LINT : 01

[132h 0306   1]                Subtable Type : 04 [Local APIC NMI]
[133h 0307   1]                       Length : 06
[134h 0308   1]                 Processor ID : 02
[135h 0309   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[137h 0311   1]         Interrupt Input LINT : 01

[138h 0312   1]                Subtable Type : 04 [Local APIC NMI]
[139h 0313   1]                       Length : 06
[13Ah 0314   1]                 Processor ID : 04
[13Bh 0315   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[13Dh 0317   1]         Interrupt Input LINT : 01

[13Eh 0318   1]                Subtable Type : 04 [Local APIC NMI]
[13Fh 0319   1]                       Length : 06
[140h 0320   1]                 Processor ID : 06
[141h 0321   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[143h 0323   1]         Interrupt Input LINT : 01

[144h 0324   1]                Subtable Type : 04 [Local APIC NMI]
[145h 0325   1]                       Length : 06
[146h 0326   1]                 Processor ID : 08
[147h 0327   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[149h 0329   1]         Interrupt Input LINT : 01

[14Ah 0330   1]                Subtable Type : 04 [Local APIC NMI]
[14Bh 0331   1]                       Length : 06
[14Ch 0332   1]                 Processor ID : 0A
[14Dh 0333   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[14Fh 0335   1]         Interrupt Input LINT : 01

[150h 0336   1]                Subtable Type : 04 [Local APIC NMI]
[151h 0337   1]                       Length : 06
[152h 0338   1]                 Processor ID : 0C
[153h 0339   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[155h 0341   1]         Interrupt Input LINT : 01

[156h 0342   1]                Subtable Type : 04 [Local APIC NMI]
[157h 0343   1]                       Length : 06
[158h 0344   1]                 Processor ID : 0E
[159h 0345   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[15Bh 0347   1]         Interrupt Input LINT : 01

[15Ch 0348   1]                Subtable Type : 04 [Local APIC NMI]
[15Dh 0349   1]                       Length : 06
[15Eh 0350   1]                 Processor ID : 10
[15Fh 0351   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[161h 0353   1]         Interrupt Input LINT : 01

[162h 0354   1]                Subtable Type : 04 [Local APIC NMI]
[163h 0355   1]                       Length : 06
[164h 0356   1]                 Processor ID : 12
[165h 0357   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[167h 0359   1]         Interrupt Input LINT : 01

[168h 0360   1]                Subtable Type : 04 [Local APIC NMI]
[169h 0361   1]                       Length : 06
[16Ah 0362   1]                 Processor ID : 14
[16Bh 0363   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[16Dh 0365   1]         Interrupt Input LINT : 01

[16Eh 0366   1]                Subtable Type : 04 [Local APIC NMI]
[16Fh 0367   1]                       Length : 06
[170h 0368   1]                 Processor ID : 16
[171h 0369   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[173h 0371   1]         Interrupt Input LINT : 01

[174h 0372   1]                Subtable Type : 04 [Local APIC NMI]
[175h 0373   1]                       Length : 06
[176h 0374   1]                 Processor ID : 18
[177h 0375   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[179h 0377   1]         Interrupt Input LINT : 01

[17Ah 0378   1]                Subtable Type : 04 [Local APIC NMI]
[17Bh 0379   1]                       Length : 06
[17Ch 0380   1]                 Processor ID : 1A
[17Dh 0381   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[17Fh 0383   1]         Interrupt Input LINT : 01

[180h 0384   1]                Subtable Type : 04 [Local APIC NMI]
[181h 0385   1]                       Length : 06
[182h 0386   1]                 Processor ID : 1C
[183h 0387   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[185h 0389   1]         Interrupt Input LINT : 01

[186h 0390   1]                Subtable Type : 04 [Local APIC NMI]
[187h 0391   1]                       Length : 06
[188h 0392   1]                 Processor ID : 1E
[189h 0393   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[18Bh 0395   1]         Interrupt Input LINT : 01

[18Ch 0396   1]                Subtable Type : 04 [Local APIC NMI]
[18Dh 0397   1]                       Length : 06
[18Eh 0398   1]                 Processor ID : 01
[18Fh 0399   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[191h 0401   1]         Interrupt Input LINT : 01

[192h 0402   1]                Subtable Type : 04 [Local APIC NMI]
[193h 0403   1]                       Length : 06
[194h 0404   1]                 Processor ID : 03
[195h 0405   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[197h 0407   1]         Interrupt Input LINT : 01

[198h 0408   1]                Subtable Type : 04 [Local APIC NMI]
[199h 0409   1]                       Length : 06
[19Ah 0410   1]                 Processor ID : 05
[19Bh 0411   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[19Dh 0413   1]         Interrupt Input LINT : 01

[19Eh 0414   1]                Subtable Type : 04 [Local APIC NMI]
[19Fh 0415   1]                       Length : 06
[1A0h 0416   1]                 Processor ID : 07
[1A1h 0417   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1A3h 0419   1]         Interrupt Input LINT : 01

[1A4h 0420   1]                Subtable Type : 04 [Local APIC NMI]
[1A5h 0421   1]                       Length : 06
[1A6h 0422   1]                 Processor ID : 09
[1A7h 0423   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1A9h 0425   1]         Interrupt Input LINT : 01

[1AAh 0426   1]                Subtable Type : 04 [Local APIC NMI]
[1ABh 0427   1]                       Length : 06
[1ACh 0428   1]                 Processor ID : 0B
[1ADh 0429   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1AFh 0431   1]         Interrupt Input LINT : 01

[1B0h 0432   1]                Subtable Type : 04 [Local APIC NMI]
[1B1h 0433   1]                       Length : 06
[1B2h 0434   1]                 Processor ID : 0D
[1B3h 0435   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1B5h 0437   1]         Interrupt Input LINT : 01

[1B6h 0438   1]                Subtable Type : 04 [Local APIC NMI]
[1B7h 0439   1]                       Length : 06
[1B8h 0440   1]                 Processor ID : 0F
[1B9h 0441   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1BBh 0443   1]         Interrupt Input LINT : 01

[1BCh 0444   1]                Subtable Type : 04 [Local APIC NMI]
[1BDh 0445   1]                       Length : 06
[1BEh 0446   1]                 Processor ID : 11
[1BFh 0447   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1C1h 0449   1]         Interrupt Input LINT : 01

[1C2h 0450   1]                Subtable Type : 04 [Local APIC NMI]
[1C3h 0451   1]                       Length : 06
[1C4h 0452   1]                 Processor ID : 13
[1C5h 0453   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1C7h 0455   1]         Interrupt Input LINT : 01

[1C8h 0456   1]                Subtable Type : 04 [Local APIC NMI]
[1C9h 0457   1]                       Length : 06
[1CAh 0458   1]                 Processor ID : 15
[1CBh 0459   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1CDh 0461   1]         Interrupt Input LINT : 01

[1CEh 0462   1]                Subtable Type : 04 [Local APIC NMI]
[1CFh 0463   1]                       Length : 06
[1D0h 0464   1]                 Processor ID : 17
[1D1h 0465   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1D3h 0467   1]         Interrupt Input LINT : 01

[1D4h 0468   1]                Subtable Type : 04 [Local APIC NMI]
[1D5h 0469   1]                       Length : 06
[1D6h 0470   1]                 Processor ID : 19
[1D7h 0471   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1D9h 0473   1]         Interrupt Input LINT : 01

[1DAh 0474   1]                Subtable Type : 04 [Local APIC NMI]
[1DBh 0475   1]                       Length : 06
[1DCh 0476   1]                 Processor ID : 1B
[1DDh 0477   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1DFh 0479   1]         Interrupt Input LINT : 01

[1E0h 0480   1]                Subtable Type : 04 [Local APIC NMI]
[1E1h 0481   1]                       Length : 06
[1E2h 0482   1]                 Processor ID : 1D
[1E3h 0483   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1E5h 0485   1]         Interrupt Input LINT : 01

[1E6h 0486   1]                Subtable Type : 04 [Local APIC NMI]
[1E7h 0487   1]                       Length : 06
[1E8h 0488   1]                 Processor ID : 1F
[1E9h 0489   2]        Flags (decoded below) : 0005
                                    Polarity : 1
                                Trigger Mode : 1
[1EBh 0491   1]         Interrupt Input LINT : 01

[1ECh 0492   1]                Subtable Type : 01 [I/O APIC]
[1EDh 0493   1]                       Length : 0C
[1EEh 0494   1]                  I/O Apic ID : 08
[1EFh 0495   1]                     Reserved : 00
[1F0h 0496   4]                      Address : FEC00000
[1F4h 0500   4]                    Interrupt : 00000000

[1F8h 0504   1]                Subtable Type : 01 [I/O APIC]
[1F9h 0505   1]                       Length : 0C
[1FAh 0506   1]                  I/O Apic ID : 09
[1FBh 0507   1]                     Reserved : 00
[1FCh 0508   4]                      Address : FEC01000
[200h 0512   4]                    Interrupt : 00000018

[204h 0516   1]                Subtable Type : 01 [I/O APIC]
[205h 0517   1]                       Length : 0C
[206h 0518   1]                  I/O Apic ID : 0A
[207h 0519   1]                     Reserved : 00
[208h 0520   4]                      Address : FEC40000
[20Ch 0524   4]                    Interrupt : 00000030

[210h 0528   1]                Subtable Type : 02 [Interrupt Source Override]
[211h 0529   1]                       Length : 0A
[212h 0530   1]                          Bus : 00
[213h 0531   1]                       Source : 00
[214h 0532   4]                    Interrupt : 00000002
[218h 0536   2]        Flags (decoded below) : 0000
                                    Polarity : 0
                                Trigger Mode : 0

[21Ah 0538   1]                Subtable Type : 02 [Interrupt Source Override]
[21Bh 0539   1]                       Length : 0A
[21Ch 0540   1]                          Bus : 00
[21Dh 0541   1]                       Source : 09
[21Eh 0542   4]                    Interrupt : 00000009
[222h 0546   2]        Flags (decoded below) : 000D
                                    Polarity : 1
                                Trigger Mode : 3

Raw Table Data: Length 548 (0x224)

  0000: 41 50 49 43 24 02 00 00 03 A1 46 55 4A 20 20 20  APIC$.....FUJ   
  0010: 44 33 32 37 39 2D 41 31 09 20 07 01 41 4D 49 20  D3279-A1. ..AMI 
  0020: 13 00 01 00 00 00 E0 FE 01 00 00 00 00 08 00 00  ................
  0030: 01 00 00 00 00 08 02 02 01 00 00 00 00 08 04 04  ................
  0040: 01 00 00 00 00 08 06 06 01 00 00 00 00 08 08 08  ................
  0050: 01 00 00 00 00 08 0A 0A 01 00 00 00 00 08 0C 0C  ................
  0060: 01 00 00 00 00 08 0E 0E 01 00 00 00 00 08 10 10  ................
  0070: 01 00 00 00 00 08 12 12 01 00 00 00 00 08 14 14  ................
  0080: 01 00 00 00 00 08 16 16 01 00 00 00 00 08 18 18  ................
  0090: 01 00 00 00 00 08 1A 1A 01 00 00 00 00 08 1C 1C  ................
  00A0: 01 00 00 00 00 08 1E 1E 01 00 00 00 00 08 01 01  ................
  00B0: 01 00 00 00 00 08 03 03 01 00 00 00 00 08 05 05  ................
  00C0: 01 00 00 00 00 08 07 07 01 00 00 00 00 08 09 09  ................
  00D0: 01 00 00 00 00 08 0B 0B 01 00 00 00 00 08 0D 0D  ................
  00E0: 01 00 00 00 00 08 0F 0F 01 00 00 00 00 08 11 11  ................
  00F0: 01 00 00 00 00 08 13 13 01 00 00 00 00 08 15 15  ................
  0100: 01 00 00 00 00 08 17 17 01 00 00 00 00 08 19 19  ................
  0110: 01 00 00 00 00 08 1B 1B 01 00 00 00 00 08 1D 1D  ................
  0120: 01 00 00 00 00 08 1F 1F 01 00 00 00 04 06 00 05  ................
  0130: 00 01 04 06 02 05 00 01 04 06 04 05 00 01 04 06  ................
  0140: 06 05 00 01 04 06 08 05 00 01 04 06 0A 05 00 01  ................
  0150: 04 06 0C 05 00 01 04 06 0E 05 00 01 04 06 10 05  ................
  0160: 00 01 04 06 12 05 00 01 04 06 14 05 00 01 04 06  ................
  0170: 16 05 00 01 04 06 18 05 00 01 04 06 1A 05 00 01  ................
  0180: 04 06 1C 05 00 01 04 06 1E 05 00 01 04 06 01 05  ................
  0190: 00 01 04 06 03 05 00 01 04 06 05 05 00 01 04 06  ................
  01A0: 07 05 00 01 04 06 09 05 00 01 04 06 0B 05 00 01  ................
  01B0: 04 06 0D 05 00 01 04 06 0F 05 00 01 04 06 11 05  ................
  01C0: 00 01 04 06 13 05 00 01 04 06 15 05 00 01 04 06  ................
  01D0: 17 05 00 01 04 06 19 05 00 01 04 06 1B 05 00 01  ................
  01E0: 04 06 1D 05 00 01 04 06 1F 05 00 01 01 0C 08 00  ................
  01F0: 00 00 C0 FE 00 00 00 00 01 0C 09 00 00 10 C0 FE  ................
  0200: 18 00 00 00 01 0C 0A 00 00 00 C4 FE 30 00 00 00  ............0...
  0210: 02 0A 00 00 02 00 00 00 00 00 02 0A 00 09 09 00  ................
  0220: 00 00 0D 00                                      ....
