#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

from . import testpaths
import pytest

from src import customExceptions
import src.extractor as extractor


class TestExtractor:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_EXTRACTOR

    # -- extractData testcases
    def test_extractData(self):
        "Tests the extractData function"

        loc = self.testdir + "testExtractData.txt"
        assert extractor.extractData(loc) == "test"
        assert extractor.extractData(loc) != "test2"
        assert extractor.extractData(loc) != ""

    def test_extractData_blank(self):
        "Tests the extractData function with blank file"

        loc = self.testdir + "testExtractData_blank.txt"
        assert extractor.extractData(loc) == ""

    def test_extractData_doesNotExist(self):
        "Tests the extractData function with file that does not exist"

        loc = self.testdir + "testExtractData_shouldnotexist.txt"
        with pytest.raises(IOError):
            extractor.extractData(loc)

    # -- extractBinaryData testcases
    def test_extractBinaryData(self):
        "Tests the extractBinaryData function"
        loc = testpaths.PATH_TEST_GEN + "testExtractBinaryData"
        with open(loc, "wb") as f:
            f.write(b"\x01\x02\x03\x04")
        assert extractor.extractBinaryData(loc, 0, 4) == "0x04030201"
        assert extractor.extractBinaryData(loc, 0, 4, chunks=True) == [
            "0x04", "0x03", "0x02", "0x01"]
        assert extractor.extractBinaryData(loc, 2, 2, chunks=True) == [
            "0x04", "0x03"]
        assert extractor.extractBinaryData(loc, 0, 2,
                                           "LITTLE_ENDIAN",
                                           chunks=True) == ["0x01", "0x02"]
        with pytest.raises(customExceptions.NoAccessToFile):
            extractor.extractBinaryData(loc, 3, 2)
        with pytest.raises(ValueError):
            extractor.extractBinaryData(
                loc, 0, 4, "MIDDLE_ENDIAN", chunks=True)
