#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import re

from . import testpaths
import unittest.mock as mock
import pytest

import src.acpi as acpi
import src.dsdt as dsdt
import src.extractor as extractor


class TestDSDT:

    @pytest.fixture(autouse=True)
    def setUp(self, request):
        self.testdir = os.path.join(testpaths.PATH_TEST_DSDT)
        self.outdir = os.path.join(testpaths.PATH_TEST_GEN + "/dsdt")

        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

        prt_src = os.path.join(self.testdir, "prt.txt")
        self.prt_data = extractor.extractData(prt_src)

        def tearDown():
            dsdt._prt = []
        request.addfinalizer(tearDown)

    def test_parse_pci_routing_table(self):
        dsdt.parse_pci_routing_table(self.prt_data)
        assert len(dsdt._prt) == 36, ("_parse_pci_routing_table: "
                                      "PRT entries mismatch")

        ref_e = dsdt.PRT_Mapping(address=0x16ffff, pin=3, source=0,
                                 source_index=0x1e)
        assert dsdt._prt[14] == ref_e, ("_parse_pci_routing_table: "
                                        "PRT entry 14 mismatch")

    def test_load_prt(self):
        dsdt.load_prt("nonexistent", self.outdir)
        assert len(dsdt._prt) == 0, ("load_prt: "
                                     "Loaded PRT from non-existent DSDT")

        dsdt_src = os.path.join(self.testdir, "dsdt.dat")
        dsdt.load_prt(dsdt_src, self.outdir)
        assert len(dsdt._prt) == 36, "load_prt: Loaded PRT mismatch"

    def test_get_dev_nr(self):
        dsdt.parse_pci_routing_table(self.prt_data)
        assert dsdt.get_dev_nr(dsdt._prt[14]) == 0x16, ("_parse_pci_routing_"
                                                        "table: Device number"
                                                        " mismatch")

    def test_get_global_system_interrupt(self):
        dsdt.parse_pci_routing_table(self.prt_data)

        gsi = dsdt.get_global_system_interrupt(0xff, 1)
        assert gsi is None, ("_parse_pci_routing_table: "
                             "Got GSI for non-existent device/pin")

        gsi = dsdt.get_global_system_interrupt(0x16, 3)
        assert gsi == 30, "_parse_pci_routing_table: GSI mismatch (1)"

        gsi = dsdt.get_global_system_interrupt(0x16, 0)
        assert gsi == 27, "_parse_pci_routing_table: GSI mismatch (2)"
