import os
import shutil

import pytest

from . import testpaths

import paths
from src import bindings
from src import schemadata


@pytest.fixture(scope="session", autouse=True)
def session_setup(request):
    bindings.init(paths.SCHEMAPATH,
                  paths.SCHEMA_BINDING_PATH)
    schemadata.selectSchema("hardware_config")

    if not os.path.isdir(testpaths.PATH_TEST_GEN):
        os.mkdir(testpaths.PATH_TEST_GEN)

    def session_finalize():
        if os.path.isdir(testpaths.PATH_TEST_GEN):
            shutil.rmtree(testpaths.PATH_TEST_GEN)
    request.addfinalizer(session_finalize)
