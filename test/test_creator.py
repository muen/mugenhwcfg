#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os

import unittest.mock as mock
import pytest

import src.creator as creator
import src.dmar as dmar
import src.fadt as fadt
import src.madt as madt
import src.devicecap as devicecap
import src.schemadata
import src.util as util
import src.output as output
import src.msrs as msrs
from src import parseutil
from src import customExceptions

from . import testpaths

from .testutil import assert_xb, assert_xbl

mock_mcfg_entries = [src.mcfg.MCFG_Entry(address=0xe0000000, segment=0,
                                         start=0, end=0x3f)]

cpuid_data = """
CPU:
0x00000001 0x0b: eax=0x00000016 ebx=0x756e6547 ecx=0x6c65746e edx=0x49656e69
0x00000002 0x03: eax=0x000806ea ebx=0x04100800 ecx=0x7ffafbff edx=0xbfebfbff
"""


class TestCreator:

    def setUp(self):
        self.testdir = testpaths.PATH_TEST_CREATOR

    def test_create_elements(setUp):

        def mock_dmesg(arg, *args):
            pass

        def mock_madt(arg, *args):
            pass

        def mock_dsdt(arg, *args):
            pass

        def mock_dmar(arg, *args):
            pass

        def mock_fadt(arg, *args):
            pass

        def mock_proc_create(arg, *args):
            cpu = src.schemadata.schema.processorType(
                cpuCores=12, speed=2000000, vmxTimerRate=12)
            for i in range(0, 8, 2):
                cpu.append(src.schemadata.schema.cpuCoreType(apicId=i))
            cpu.append(src.schemadata.schema.cpuIDValueType(
                leaf='16#0000_0001#',
                subleaf='16#01#',
                eax='16#3769_2029#', ebx='16#2c10_0800#',
                ecx='16#3536_382d#', edx='16#0249_0002#'))
            cpu.append(src.schemadata.schema.msrValueType(
                address='16#0000_01a0#', name='SOME_MSR#',
                regval='16#1234_5678_abcd_efab#'))
            return cpu

        def mock_mem_create(arg, *args):
            return src.schemadata.schema.physicalMemoryType()

        def mock_dev_create(arg, *args):
            return src.schemadata.schema.devicesType()

        @mock.patch("src.creator.genDmesg", mock_dmesg)
        @mock.patch("src.madt.parse_table", mock_madt)
        @mock.patch("src.dmar.parse_table", mock_dmar)
        @mock.patch("src.dsdt.load_prt", mock_dsdt)
        @mock.patch("src.fadt.parse_table", mock_fadt)
        @mock.patch("src.creator.ProcessorCreator.createElem",
                    mock_proc_create)
        @mock.patch("src.creator.MemoryCreator.createElem", mock_mem_create)
        @mock.patch("src.creator.DevicesCreator.createElem", mock_dev_create)
        def execute():
            hardware = creator.createElements()
            assert hardware.validateBinding() is True
        execute()


class TestProcessorCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(
            testpaths.PATH_TEST_CREATOR, "processorcreator")
        self.procreator = creator.ProcessorCreator()

    def test_createElem(self):
        def mock_cpuid(*args):
            return cpuid_data

        def mock_read_msr(msrpaths, offset, optional, *args):
            return '0x1234'

        @mock.patch.object(output, "_runCommand", mock_cpuid)
        @mock.patch.object(msrs, "read_msr", mock_read_msr)
        def execute():

            cpuinfo = os.path.join(self.testdir, "cpuinfo")
            msr = os.path.join(testpaths.PATH_TEST_GEN, "msr")
            dmesg = os.path.join(self.testdir, "dmesg")

            ref = src.schemadata.schema.processorType(cpuCores=4,
                                                      speed=3192746,
                                                      vmxTimerRate=20)
            for i in range(0, 8, 2):
                ref.append(src.schemadata.schema.cpuCoreType(apicId=i))
            ref.append(src.schemadata.schema.cpuIDValueType(
                leaf='16#0000_0001#', subleaf='16#0b#',
                eax='16#0000_0016#', ebx='16#756e_6547#',
                ecx='16#6c65_746e#', edx='16#4965_6e69#'))
            ref.append(src.schemadata.schema.cpuIDValueType(
                leaf='16#0000_0002#', subleaf='16#03#',
                eax='16#0008_06ea#', ebx='16#0410_0800#',
                ecx='16#7ffa_fbff#', edx='16#bfeb_fbff#'))
            for m in msrs.MSRs_Of_Interest:
                ref.append(src.schemadata.schema.msrValueType(
                    address=m['address'], name=m['name'],
                    regval='16#0000_0000_0000_1234#'))

            result = self.procreator.createElem(cpuinfo, [msr], dmesg)
            assert_xb(result, ref, "processor")

        execute()

    def test_get_cpu_cores(self):
        cpuinfoloc = os.path.join(self.testdir, "cpuinfo")
        assert self.procreator.get_cpu_cores(cpuinfoloc) == 4

        cpuinfoloc = os.path.join(self.testdir, "cpuinfo_multisocket")
        assert self.procreator.get_cpu_cores(cpuinfoloc) == 16

    def test_get_apic_ids(self):
        cpuinfoloc = os.path.join(self.testdir, "cpuinfo_multisocket")
        assert len(self.procreator.get_apic_ids(cpuinfoloc)) == 16

    def test_getSpeed(self):
        dmesgloc = os.path.join(self.testdir, "dmesg")
        dmesgloc2 = os.path.join(self.testdir, "dmesg_tsc_detected")
        dmesg_nokey = os.path.join(self.testdir, "dmesg_nokey")
        invalidloc = os.path.join(self.testdir, "invalidlocdmesg")
        assert self.procreator.getSpeed(dmesgloc) == 3192746
        assert self.procreator.getSpeed(dmesgloc2) == 3192667
        with pytest.raises(customExceptions.ForceQuit):
            self.procreator.getSpeed(invalidloc)
        with pytest.raises(customExceptions.ForceQuit):
            self.procreator.getSpeed(dmesg_nokey)

    def test_getVmxTimerRate(self):
        msrpath1 = os.path.join(testpaths.PATH_TEST_GEN, "testmsr_path1")
        msrpath_invalid = "invalidpath"
        msrpathlist = [msrpath_invalid]
        OFFSET = 0
        VMX_BITSIZE = 5
        with pytest.raises(customExceptions.ForceQuit):
            self.procreator.getVmxTimerRate(msrpathlist, OFFSET, VMX_BITSIZE)

        with open(msrpath1, "wb") as f:
            f.write(b"\x01\x02\x03\x04")
        msrpathlist.append(msrpath1)

        assert self.procreator.getVmxTimerRate(msrpathlist, OFFSET,
                                               VMX_BITSIZE) == 1


class TestMemoryCreator():

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(
            testpaths.PATH_TEST_CREATOR, "memorycreator")
        self.memcreator = creator.MemoryCreator()

    def test_createElem(self):

        def mock_dmar_get_reserved_mem_regions():
            return [dmar.Rmrr_Region(name="rmrr23",
                                     start="0x1f0000", end="0x1f1000")]

        with mock.patch('src.dmar.get_reserved_mem_regions',
                        side_effect=mock_dmar_get_reserved_mem_regions):
            result = self.memcreator.createElem(os.path.join(self.testdir,
                                                "memmap"))
            assert len(result.memoryBlock) == 2
            assert len(result.reservedMemory) == 1
            assert result.reservedMemory[0].name == "rmrr23"

    def test_memmapextraction(self):
        "Tests various functions related to memmap extraction"

        loc = os.path.join(self.testdir, "memmap")
        invalidloc = os.path.join(self.testdir, "memmap_invalid")
        incompleteloc = os.path.join(self.testdir, "memmap_incomplete")
        casing = os.path.join(self.testdir, "casing")

        # Test isAllocatable function
        mem1_name = "System RAM"
        mem1_addr = "0x0010000f"

        mem2_name = "System RAM2"
        mem2_addr = "0x0010000f"

        mem3_name = "System RAM"
        mem3_addr = "0xfffff"

        mem4_name = "System RAM2"
        mem4_addr = "0x1000"

        assert self.memcreator.isAllocatable(mem1_name, mem1_addr) is True
        assert self.memcreator.isAllocatable(mem2_name, mem2_addr) is False
        assert self.memcreator.isAllocatable(mem3_name, mem3_addr) is False
        assert self.memcreator.isAllocatable(mem4_name, mem4_addr) is False

        # Test getMemoryBlocks
        memoryBlockList_invalid = self.memcreator.getMemoryBlocks(invalidloc)
        assert not memoryBlockList_invalid
        memoryBlockList_incomplete = self.memcreator.getMemoryBlocks(
            incompleteloc)
        assert not memoryBlockList_incomplete
        memoryBlockList_casing = self.memcreator.getMemoryBlocks(casing)
        assert not memoryBlockList_casing

        memoryBlockList = self.memcreator.getMemoryBlocks(loc)
        assert len(memoryBlockList) == 2

        memoryBlock0 = memoryBlockList[0]
        memoryBlock2 = memoryBlockList[1]
        assert memoryBlock0.name == "0_type"
        assert memoryBlock0.physicalAddress == "16#000a#"
        assert memoryBlock0.size == "16#f000#"
        assert memoryBlock2.name == "2_type"
        assert memoryBlock2.physicalAddress == "16#3000#"
        assert memoryBlock2.size == "16#6000#"

        # Test generateMemoryBlock
        startfile = os.path.join(loc, "0/start")
        startfile_invalid = os.path.join(loc, "0/start_invalid")
        endfile = os.path.join(loc, "0/end")
        typefile = os.path.join(loc, "0/type")

        memoryBlock = self.memcreator.generateMemoryBlock(
            endfile, typefile, startfile)
        assert memoryBlock.name == "0_type"
        assert memoryBlock.physicalAddress == "16#000a#"
        with pytest.raises(IOError):
            self.memcreator.generateMemoryBlock(endfile, typefile,
                                                startfile_invalid)

        def isAllocatable_true(name, physical_addr):
            return True

        @mock.patch.object(self.memcreator,
                           "isAllocatable",
                           isAllocatable_true)
        def mock_testAllocatable():
            return self.memcreator.generateMemoryBlock(endfile,
                                                       typefile,
                                                       startfile)
        mockmemblock = mock_testAllocatable()
        assert mockmemblock.allocatable == "true"

    def test_generate_reserved_mem_region(self):
        rmrr = dmar.Rmrr_Region(name="region1",
                                start="0xafe0000",
                                end="0xdeadbfff")

        reg = self.memcreator.generate_reserved_mem_region(rmrr)
        assert reg.name == "region1"
        assert reg.physicalAddress == "16#0afe_0000#"
        assert reg.size == "16#d3af_c000#"


class TestDevicesCreator:

    def mock_sysboard_create_element(self):
        return src.schemadata.schema.deviceType(name="dummy")

    def mock_get_mcfg_entries():
        return [src.mcfg.MCFG_Entry(address=0xe0000000, segment=0,
                                    start=0, end=0x3f)]

    @mock.patch.object(src.mcfg, "_entries", mock_mcfg_entries)
    @mock.patch("src.creator.IoapicDevicesCreator.createElems")
    @mock.patch("src.creator.IommuDevicesCreator.createElems")
    @mock.patch("src.creator.SystemboardDeviceCreator.create_element",
                mock_sysboard_create_element)
    @mock.patch("src.creator.SerialDevicesCreator.createElems")
    @mock.patch("src.creator.PciDevicesCreator.createElems")
    def test_createElem(self, mock_ioapic, mock_iommu, mock_serial, mock_pci):
        creator.DevicesCreator().createElem("dmesgpath")
        assert mock_ioapic.called
        assert mock_iommu.called
        assert mock_serial.called
        assert mock_pci.called

    @mock.patch.object(src.mcfg, "_entries", mock_mcfg_entries)
    def test_getPciConfigAddress(self):
        assert creator.getPciConfigAddress() == "0xe0000000"

    def test_getPciConfigSize(self):
        mock_mcfg_invalid_entry = [src.mcfg.MCFG_Entry(address=0xe0000000,
                                   segment=0, start=0x1, end=0x0)]

        @mock.patch.object(src.mcfg, "_entries", mock_mcfg_entries)
        def positive_test():
            assert creator.getPciConfigSize() == "0x4000000"

        @mock.patch.object(src.mcfg, "_entries", mock_mcfg_invalid_entry)
        def invalid_start_end_test():
            assert creator.getPciConfigSize() == ""

        positive_test()
        invalid_start_end_test()


class TestPciDevicesCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(
            testpaths.PATH_TEST_CREATOR, "devicescreator")
        self.devloc = os.path.join(self.testdir, "devices")
        self.devtree = os.path.join(self.testdir, "devices_test")
        self.devicesdir = testpaths.PATH_DEVICELINKS
        self.pcicreator = creator.PciDevicesCreator()

    @mock.patch.object(src.mcfg, "_entries", mock_mcfg_entries)
    def test_createElems(self):
        devicesdir = os.path.join(self.testdir, "devices_test_links")
        assert len(self.pcicreator.createElems(devicesdir,
                                               testpaths.PATH_IOMMUGRP)) == 2

    def test_init_DevicecapManager(self):
        testloc = self.devtree
        devices = os.listdir(testloc)
        devicepaths = []
        for devicename in devices:
            devicepaths.append(os.path.join(testloc, devicename))

        valid = self.pcicreator.init_DevicecapManager(devicepaths)
        assert len(valid.getCapabilities()) == 2

        devicecapmgr_invalid = self.pcicreator.init_DevicecapManager(
            "invaliddevicepaths")
        assert len(devicecapmgr_invalid.getCapabilities()) == 0

    def test_filterDevicePaths(self):
        testloc = self.devtree
        devices = os.listdir(testloc)
        devicepaths = []
        for devicename in devices:
            devicepaths.append(os.path.join(testloc, devicename))

        devicecapmgr = devicecap.DevicecapManager()
        devicecapmgr.extractCapabilities(devicepaths)

        assert len(self.pcicreator.filterDevicePaths(devicepaths,
                                                     devicecapmgr)) == 2

    def test_isDeviceName(self):
        assert self.pcicreator.isDeviceName("0000:01:00.0") is True
        assert self.pcicreator.isDeviceName("0000:00:01.2") is True
        assert self.pcicreator.isDeviceName("0001:17:02.5") is True
        assert self.pcicreator.isDeviceName("010:10:01.2") is False
        assert self.pcicreator.isDeviceName("0000:10:01") is False
        assert self.pcicreator.isDeviceName("000:10:01.1") is False
        assert self.pcicreator.isDeviceName("000:10:010.1") is False
        assert self.pcicreator.isDeviceName("000:10:01.11") is False
        assert self.pcicreator.isDeviceName("000:10:01:00.1") is False
        assert self.pcicreator.isDeviceName("0003:0E0F:0003.0001") is False

    def test_isBridge(self):
        assert self.pcicreator.isBridge(
            os.path.join(self.devloc, "pcibridge0")) is True
        assert self.pcicreator.isBridge(
            os.path.join(self.devloc, "dev0")) is False

    def test_isPciExpress(self):
        dev0 = os.path.join(self.testdir, "devices_testcap/dev0")
        dev1 = os.path.join(self.testdir, "devices_testcap/dev1")
        devicecapmgr = devicecap.DevicecapManager()
        devicecapmgr.extractCapabilities([dev0, dev1])
        assert self.pcicreator.isPciExpress(dev0, devicecapmgr) is True
        assert self.pcicreator.isPciExpress(dev1, devicecapmgr) is False

    def test_getDeviceBus(self):
        assert self.pcicreator.getDeviceBus("0011:01:02.3") == "01"

    def test_getDeviceNo(self):
        assert self.pcicreator.getDeviceNo("0000:01:02.3") == "02"

    def test_getDeviceFunction(self):
        assert self.pcicreator.getDeviceFunction("0000:01:02.3") == "3"

    def test_getDeviceSpecs(self):
        testpciids = testpaths.PATH_PCIIDS
        devdir = os.path.join(self.testdir, "devices_testspecs")
        devpaths = []
        for dir in sorted(os.listdir(devdir)):
            devpaths.append(os.path.join(devdir, dir))
        testresult = {
            os.path.join(devdir, "dev0"):
                creator.PciDevice(
                    name="host_bridge_1",
                    classcode="16#0600#",
                    vendorId="16#8086#",
                    deviceId="16#0082#",
                    revisionId="16#a1#",
                    descr="Intel Corporation Centrino Advanced-N 6205 "
                    + "[Taylor Peak]"),
            os.path.join(devdir, "dev1"):
                creator.PciDevice(
                    name="06ff_1",
                    classcode="16#06ff#",
                    vendorId="16#102f#",
                    deviceId="16#01b5#",
                    revisionId="16#05#",
                    descr="Toshiba America SCC USB 2.0 EHCI controller"),
            os.path.join(devdir, "dev2"):
                creator.PciDevice(
                    name="pci_bridge_1",
                    classcode="16#0604#",
                    vendorId="16#0e11#",
                    deviceId="16#1000#",
                    revisionId="16#67#",
                    descr="Compaq Computer Corporation Triflex/Pentium Bridge,"
                    + " Model 1000"),
            os.path.join(devdir, "unknown"):
                creator.PciDevice(
                    name="fa00_1",
                    classcode="16#fa00#",
                    vendorId="16#9876#",
                    deviceId="16#1234#",
                    revisionId="16#00#",
                    descr="")
        }

        result = self.pcicreator.getDeviceSpecs(devpaths, testpciids)
        assert result == testresult
        specs = self.pcicreator.getDeviceSpecs(devpaths,
                                               "invalidpciidsloc")
        assert len(specs) == 0

    def test_toClassName(self):
        testpciids = testpaths.PATH_PCIIDS
        pciidsparser = parseutil.PciIdsParser(testpciids)
        assert self.pcicreator.toClassName("0x0600",
                                           pciidsparser) == "host_bridge"
        assert self.pcicreator.toClassName("0x1234",
                                           pciidsparser) == "0x1234"

    def test_getPci(self):
        testloc = os.path.join(self.testdir, "devices_testcap")
        devicecapmgr = devicecap.DevicecapManager()
        dev = os.path.join(testloc, "0000:01:02.3")
        devicecapmgr.extractCapabilities([dev])

        devicespec = creator.PciDevice(
            name="testdevice",
            classcode="16#0403#",
            vendorId="16#2222#",
            deviceId="16#1112#",
            revisionId="16#a1#",
            descr="Some device")

        dev_pci = self.pcicreator.getPci(dev, devicecapmgr, devicespec,
                                         testpaths.PATH_IOMMUGRP)
        assert dev_pci.bus == "16#01#"
        assert dev_pci.function == 3
        assert dev_pci.device == "16#02#"
        assert dev_pci.identification.classcode == "16#0403#"
        assert dev_pci.identification.vendorId == "16#2222#"
        assert dev_pci.identification.deviceId == "16#1112#"
        assert dev_pci.identification.revisionId == "16#a1#"
        assert dev_pci.iommuGroup.id == 2

    def test_getLegacyIrq(self):
        testloc = os.path.join(self.testdir, "devices_testirq")

        def mock_get_int_info(path):
            intInfo = {
                '0000:00:00.0': devicecap.Interrupt_Info(pin=1, line=255),
                '0000:00:01.0': devicecap.Interrupt_Info(pin=2, line=15),
                '0000:00:13.0': devicecap.Interrupt_Info(pin=3, line=18),
                '0000:00:1f.3': devicecap.Interrupt_Info(pin=4, line=19)
            }
            result = devicecap.Interrupt_Info(line=0, pin=255)
            if path in intInfo:
                result = intInfo[path]
            return result

        mk = mock.Mock()
        mk.getInterruptInfo = mock_get_int_info

        mock_prt = [
            src.dsdt.PRT_Mapping(address=0x01ffff, pin=1, source=0,
                                 source_index=15),
            src.dsdt.PRT_Mapping(address=0x13ffff, pin=2, source=0,
                                 source_index=18),
            src.dsdt.PRT_Mapping(address=0x1fffff, pin=3, source=0,
                                 source_index=19),
        ]

        with mock.patch("src.dsdt._prt", mock_prt):
            dev0 = "0000:00:00.0"
            dev1 = "0000:00:01.0"
            dev4 = "0000:00:1f.3"
            dev5 = "0000:00:13.0/0000:03:00.0"
            dev_invalid = "0000:00:1c.5"

            dev0_irq = self.pcicreator.getLegacyIrq(dev0, mk)
            dev1_irq = self.pcicreator.getLegacyIrq(dev1, mk)
            dev4_irq = self.pcicreator.getLegacyIrq(dev4, mk)
            dev5_irq = self.pcicreator.getLegacyIrq(dev5, mk)
            dev_invalid_irq = self.pcicreator.getLegacyIrq(dev_invalid, mk)

            assert dev1_irq[0].number == 15
            assert dev1_irq[0].name == "irq1"
            assert dev4_irq[0].number == 19
            assert dev4_irq[0].name == "irq1"
            assert dev5_irq[0].number == 18
            assert dev5_irq[0].name == "irq1"

            assert not dev0_irq
            assert not dev_invalid_irq

    def test_getDeviceMemory(self):
        testloc = os.path.join(self.testdir, "devices_testresource")

        # No filter: actual data
        dev0_loc = os.path.join(testloc, "dev0")
        dev2_loc = os.path.join(testloc, "dev2")
        dev_emptyresource = os.path.join(testloc, "dev_emptyresource")
        dev_invalidresource = os.path.join(testloc, "dev_invalidresource")

        dev0_testmemblock_nofilter = [
            src.schemadata.schema.deviceMemoryType(
                name="mem1", physicalAddress="16#fb22_5000#", size="16#0800#",
                caching="UC")]
        dev2_testmemblock_nofilter = [
            src.schemadata.schema.deviceMemoryType(
                name="mem1", physicalAddress="16#fb20_0000#",
                size="16#0002_0000#", caching="UC"),
            src.schemadata.schema.deviceMemoryType(
                name="mem2", physicalAddress="16#fb22_8000#", size="16#1000#",
                caching="UC")
        ]

        NOFILTERSIZE = "0x0"
        dev0_memblocks_nofilter = self.pcicreator.getDeviceMemory(
            dev0_loc, NOFILTERSIZE)
        dev2_memblocks_nofilter = self.pcicreator.getDeviceMemory(
            dev2_loc, NOFILTERSIZE)

        assert_xbl(dev0_memblocks_nofilter,
                   dev0_testmemblock_nofilter, "memory")
        assert_xbl(dev2_memblocks_nofilter,
                   dev2_testmemblock_nofilter, "memory")

        # Filter on
        dev0_testmemblock_filter = [
            src.schemadata.schema.deviceMemoryType(
                name="mem1", physicalAddress="16#fb22_5000#", size="16#1000#",
                caching="UC")]
        dev0_memblocks_filter = self.pcicreator.getDeviceMemory(dev0_loc)

        assert_xbl(dev0_memblocks_filter, dev0_testmemblock_filter, "memory")

        # Test empty resource
        dev_emptyresource_list = self.pcicreator.getDeviceMemory(
            dev_emptyresource)
        assert dev_emptyresource_list == []

        # Test invalid resource
        dev_invalidresourcelist = self.pcicreator.getDeviceMemory(
            dev_invalidresource)
        assert dev_invalidresourcelist == []

    def test_getIoports(self):
        testloc = os.path.join(self.testdir, "devices_testresource")
        dev0_loc = os.path.join(testloc, "dev0")
        dev1_loc = os.path.join(testloc, "dev1")
        dev_noresource = os.path.join(testloc, "dev_noresource")
        dev_emptyresource = os.path.join(testloc, "dev_emptyresource")
        dev0_testioports = [
            src.schemadata.schema.ioPortType(
                name="ioport1", start="16#f090#", end="16#f097#"),
            src.schemadata.schema.ioPortType(
                name="ioport2", start="16#f080#", end="16#f083#"),
            src.schemadata.schema.ioPortType(
                name="ioport3", start="16#f070#", end="16#f077#"),
            src.schemadata.schema.ioPortType(
                name="ioport4", start="16#f060#", end="16#f063#"),
            src.schemadata.schema.ioPortType(
                name="ioport5", start="16#f020#", end="16#f03f#")
        ]
        dev1_testioports = []
        dev_emptyresource_testioports = []

        dev0_ioports = self.pcicreator.getIoports(dev0_loc)
        dev1_ioports = self.pcicreator.getIoports(dev1_loc)
        dev_emptyresource_ioports = self.pcicreator.getIoports(
            dev_emptyresource)

        assert_xbl(dev0_ioports, dev0_testioports, "ioPort")
        assert_xbl(dev1_ioports, dev1_testioports, "ioPort")
        assert dev_emptyresource_ioports == dev_emptyresource_testioports
        assert self.pcicreator.getIoports(dev_noresource) == []

    def test_getIrqs(self):
        testloc = os.path.join(self.testdir, "devices_testirq")
        dev0_loc = os.path.join(testloc, "0000:00:00.0")
        dev1_loc = os.path.join(testloc, "0000:00:01.0")
        dev2_loc = os.path.join(testloc, "0000:00:02.0")
        dev3_loc = os.path.join(testloc, "0000:00:13.0")
        dev4_loc = os.path.join(testloc, "0000:00:1a.0")
        dev_emptyresource = os.path.join(testloc, "0000:00:1c.5")
        dev1_testirqs = [src.schemadata.schema.irqType(name="irq1", number=15)]
        dev2_testirqs = [src.schemadata.schema.irqType(name="irq1", number=18)]
        irq3 = src.schemadata.schema.irqType(name="irq1", number=19)
        irq4 = src.schemadata.schema.irqType(
                name="irq1", number=creator.DEFAULT_IRQ_NUMBER)
        for i in range(1, 9):
            irq3.append(src.schemadata.schema.msiIrqType(name="msi" + str(i)))
            irq4.append(src.schemadata.schema.msiIrqType(name="msi" + str(i)))
        dev3_testirqs = [irq3]
        dev4_testirqs = [irq4]

        mock_prt = [
            src.dsdt.PRT_Mapping(address=0x01ffff, pin=0, source=0,
                                 source_index=15),
            src.dsdt.PRT_Mapping(address=0x02ffff, pin=0, source=0,
                                 source_index=18),
            src.dsdt.PRT_Mapping(address=0x13ffff, pin=0, source=0,
                                 source_index=19),
            src.dsdt.PRT_Mapping(address=0x1affff, pin=0, source=0,
                                 source_index=0),
            src.dsdt.PRT_Mapping(address=0x1fffff, pin=3, source=0,
                                 source_index=18),
        ]

        with mock.patch("src.dsdt._prt", mock_prt):
            devcapmgr = devicecap.DevicecapManager()
            devcapmgr.extractCapabilities([dev1_loc, dev2_loc, dev3_loc,
                                          dev4_loc])
            dev0_irqs = self.pcicreator.getIrqs(dev0_loc, devcapmgr)
            dev1_irqs = self.pcicreator.getIrqs(dev1_loc, devcapmgr)
            dev2_irqs = self.pcicreator.getIrqs(dev2_loc, devcapmgr)
            dev3_irqs = self.pcicreator.getIrqs(dev3_loc, devcapmgr)
            dev4_irqs = self.pcicreator.getIrqs(dev4_loc, devcapmgr)
            dev_emptyresource_irqs = self.pcicreator.getIrqs(
                dev_emptyresource, devcapmgr)

            assert not dev0_irqs
            assert_xbl(dev1_irqs, dev1_testirqs, "irq")
            assert_xbl(dev2_irqs, dev2_testirqs, "irq")
            assert_xbl(dev3_irqs, dev3_testirqs, "irq")
            assert_xbl(dev4_irqs, dev4_testirqs, "irq")
            assert not dev_emptyresource_irqs

            def test_get_mmconf(self):
                mem = schemadata.schema.deviceMemoryType(
                    name="mmconf",
                    physicalAddress="16#f928_3000#",
                    size="16#1000#",
                    caching="UC")
                assert_xb(mem, self.pcicreator.get_mmconf(0xf9000000, 2,
                                                          16, 3))

    @mock.patch.object(src.mcfg, "_entries",
                       [src.mcfg.MCFG_Entry(address=0xf8000000, segment=0,
                                            start=0, end=0x3f)])
    def test_createDeviceFromPath(self):
        # PCI config base not readable as non-root, set to known value.
        mmconfaddr = "16#f800_2000#"

        testloc = os.path.join(self.testdir, "devices_test")
        devpath = os.path.join(testloc, "0000:00:1f.3")
        refdev = src.schemadata.schema.deviceType(name="smbus_1")
        refdev.description = ("Intel Corporation 6 Series/C200 Series " +
                              "Chipset Family SMBus Controller")
        pci = src.schemadata.schema.pciType(
            bus="16#00#",
            device="16#1f#",
            function=3).append(
                src.schemadata.schema.deviceIdentificationType(
                    classcode="16#0c05#",
                    deviceId="16#1c22#",
                    vendorId="16#8086#",
                    revisionId="16#04#"))
        irq = src.schemadata.schema.irqType(name="irq1", number=19)
        rmrr = src.schemadata.schema.namedRefType(ref="rmrr42")
        ioport = src.schemadata.schema.ioPortType(
            name="ioport1", start="16#f000#", end="16#f01f#")

        refdev.append(pci)
        refdev.append(irq)
        refdev.append(src.schemadata.schema.deviceMemoryType(
            caching="UC", name="mem1", physicalAddress="16#fb22_4000#",
            size="16#1000#"))
        refdev.append(src.schemadata.schema.deviceMemoryType(
            caching="UC", name="mmconf", physicalAddress=mmconfaddr,
            size="16#1000#"))
        refdev.append(ioport)
        refdev.append(rmrr)

        mock_prt = [
            src.dsdt.PRT_Mapping(address=0x01ffff, pin=1, source=0,
                                 source_index=15),
            src.dsdt.PRT_Mapping(address=0x13ffff, pin=2, source=0,
                                 source_index=18),
            src.dsdt.PRT_Mapping(address=0x1fffff, pin=2, source=0,
                                 source_index=19),
        ]

        with mock.patch("src.dsdt._prt", mock_prt):
            devcapmgr = devicecap.DevicecapManager()
            devcapmgr.extractCapabilities([devpath])
            devspecs = self.pcicreator.getDeviceSpecs(
                [devpath], testpaths.PATH_PCIIDS)

            dmar._devices.append(dmar.PCI_Dev(bus=0, device=0x1f, function=3,
                                 rmrr_name="rmrr42"))
            resultdev = self.pcicreator.createDeviceFromPath(
                devpath, devcapmgr, devspecs[devpath], None)
            # PCI config base not readable as non-root, set to known value
            resultdev.memory[1].physicalAddress = mmconfaddr
            assert_xb(resultdev, refdev, "device")


class TestSerialDevicesCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(
            testpaths.PATH_TEST_CREATOR, "devicescreator")
        self.serialcreator = creator.SerialDevicesCreator()

    def test_createElems(self):
        result = self.serialcreator.createElems(testpaths.PATH_SERIALS)

        assert len(result) == 4

        device = src.schemadata.schema.deviceType(name="com_1")
        device.append(src.schemadata.schema.irqType(
            name="irq1", number=4))
        device.append(src.schemadata.schema.ioPortType(
            name="ioport1", start="16#03f8#", end="16#03ff#"))
        assert_xb(result[0], device, "com_1")

        device = src.schemadata.schema.deviceType(name="com_2")
        device.append(src.schemadata.schema.irqType(
            name="irq1", number=3))
        device.append(src.schemadata.schema.ioPortType(
            name="ioport1", start="16#02f8#", end="16#02ff#"))
        assert_xb(result[1], device, "com_2")

        device = src.schemadata.schema.deviceType(name="com_3")
        device.append(src.schemadata.schema.irqType(
            name="irq1", number=7))
        device.append(src.schemadata.schema.ioPortType(
            name="ioport1", start="16#03e8#", end="16#03ef#"))
        assert_xb(result[2], device, "com_3")

        device = src.schemadata.schema.deviceType(name="com_4")
        device.append(src.schemadata.schema.irqType(
            name="irq1", number=10))
        device.append(src.schemadata.schema.ioPortType(
            name="ioport1", start="16#02e8#", end="16#02ef#"))
        assert_xb(result[3], device, "com_4")

    def test_parseSerialDeviceResources(self):
        ref1 = creator.SerialDevice(name="com_1",
                                    start="0x3f8", end="0x3ff",
                                    irq="4")
        ref2 = creator.SerialDevice(name="serial_port_1",
                                    start="0x50b0", end="0x50b7",
                                    irq=None)
        resource1 = "status=active\nio 0x3f8-0x3ff\nirq 4"
        resource2 = "status=active\nio 0x50b0-0x50b7"

        assert self.serialcreator.parseSerialDeviceResources(resource1) == ref1
        assert self.serialcreator.parseSerialDeviceResources(resource2) == ref2
        assert self.serialcreator.parseSerialDeviceResources("") is None
        assert self.serialcreator.parseSerialDeviceResources(
            "invalid resouce") is None


class TestSystemboardDeviceCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.sysboardcreator = creator.SystemboardDeviceCreator()

    def test_create_element(self):

        def mock_get_shutdown_port():
            return "0x00001804"

        @mock.patch.object(fadt, "get_shutdown_port",
                           mock_get_shutdown_port)
        def mock_create_element():
            return self.sysboardcreator.create_element()

        result = mock_create_element()

        dev = src.schemadata.schema.deviceType(name="system_board")
        dev.append(src.schemadata.schema.ioPortType(
            name="reset",
            start=util.toWord64(hex(0xcf9)),
            end=util.toWord64(hex(0xcf9))))

        dev.append(src.schemadata.schema.ioPortType(
            name="pm1a_cnt",
            start="16#1804#",
            end="16#1804#"))

        caps = src.schemadata.schema.capabilitiesType()
        caps.append(src.schemadata.schema.capabilityType(name="systemboard"))
        caps.append(src.schemadata.schema.capabilityType(
            7168, name="pm1a_cnt_slp_typ"))
        dev.append(caps)
        assert_xb(result, dev, "system_board")


class TestIoapicDevicesCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(testpaths.PATH_TEST_CREATOR, "ioapic")
        self.ioapiccreator = creator.IoapicDevicesCreator()

    def test_createElems(self):

        def mock_get_ioapics():
            return [madt.IO_Apic(id=1, address=0xfec00000, gsi_base=0),
                    madt.IO_Apic(id=2, address=0xfec01000, gsi_base=24)]

        def mock_get_ioapic_devices():
            return [src.dmar.IO_Apic(id=2, bus=0xfa, device=0x1f,
                                     function=0x0),
                    src.dmar.IO_Apic(id=1, bus=0xf0, device=0x1f,
                                     function=0x0)]

        def mock_get_ioapic_devices_empty():
            return []

        @mock.patch.object(madt, "get_ioapics",
                           mock_get_ioapics)
        def mock_createElems():
            dmesgpath = os.path.join(self.testdir, "dmesg")
            return self.ioapiccreator.createElems(dmesgpath)

        with mock.patch('src.dmar.get_ioapic_devices',
                        side_effect=mock_get_ioapic_devices):
            result = mock_createElems()

        ref_data = []
        dev = src.schemadata.schema.deviceType(name="ioapic_1")
        dev.append(src.schemadata.schema.deviceMemoryType(
            name="mem1",
            caching="UC",
            physicalAddress=util.toWord64(hex(0xfec00000)),
            size="16#1000#"))

        caps = src.schemadata.schema.capabilitiesType()
        caps.append(src.schemadata.schema.capabilityType(name="ioapic"))
        caps.append(src.schemadata.schema.capabilityType("0", name="gsi_base"))
        caps.append(src.schemadata.schema.capabilityType("23",
                    name="max_redirection_entry"))
        caps.append(src.schemadata.schema.capabilityType("16#f0f8#",
                    name="source_id"))
        dev.append(caps)
        ref_data.append(dev)
        dev = src.schemadata.schema.deviceType(name="ioapic_2")
        dev.append(src.schemadata.schema.deviceMemoryType(
            name="mem1",
            caching="UC",
            physicalAddress=util.toWord64(hex(0xfec01000)),
            size="16#1000#"))

        caps = src.schemadata.schema.capabilitiesType()
        caps.append(src.schemadata.schema.capabilityType(name="ioapic"))
        caps.append(src.schemadata.schema.capabilityType("24",
                    name="gsi_base"))
        caps.append(src.schemadata.schema.capabilityType("16",
                    name="max_redirection_entry"))
        caps.append(src.schemadata.schema.capabilityType("16#faf8#",
                    name="source_id"))

        dev.append(caps)
        ref_data.append(dev)

        assert_xbl(result, ref_data, "ioapic")

        with mock.patch('src.dmar.get_ioapic_devices',
                        side_effect=mock_get_ioapic_devices_empty):
            res = mock_createElems()
            for ioapic in res:
                for cap in ioapic.capabilities.capability:
                    if cap.name == "source_id":
                        assert cap.value() == creator.DEFAULT_IOAPIC_SID


class TestIommuDevicesCreator:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = os.path.join(
            testpaths.PATH_TEST_CREATOR, "devicescreator")
        self.iommucreator = creator.IommuDevicesCreator()

    def test_createElems(self):

        def mock_dmarparser_getIommuAddrs():
            return ["0xe00000"]

        @mock.patch.object(dmar, "get_iommu_addrs",
                           mock_dmarparser_getIommuAddrs)
        def mock_createElems():
            return self.iommucreator.createElems("devmem")

        mock_createElems()

    def test_getIommuAGAW(self):
        testdevmem = os.path.join(testpaths.PATH_TEST_GEN, "testdevmem")
        testdevmem_invalid = "test_devmem_invalid"
        IOMMUADDR = 0x0
        CAP_OFFSET = 0x0
        CAP_REG_BYTE_SIZE = 3
        AGAW_BIT_START = 0

        with open(testdevmem, "wb") as f:
            f.write(b"\x02\x02\x03\x04")
        assert self.iommucreator.getIommuAGAW(IOMMUADDR,
                                              testdevmem,
                                              CAP_OFFSET,
                                              CAP_REG_BYTE_SIZE,
                                              AGAW_BIT_START) == "39"

        with open(testdevmem, "wb") as f:
            f.write(b"\x04\x02\x03\x04")
        assert self.iommucreator.getIommuAGAW(IOMMUADDR,
                                              testdevmem,
                                              CAP_OFFSET,
                                              CAP_REG_BYTE_SIZE,
                                              AGAW_BIT_START) == "48"
        with open(testdevmem, "wb") as f:
            f.write(b"\x01\x02\x03\x04")
        assert self.iommucreator.getIommuAGAW(IOMMUADDR,
                                              testdevmem,
                                              CAP_OFFSET,
                                              CAP_REG_BYTE_SIZE,
                                              AGAW_BIT_START) == "agaw"

        assert self.iommucreator.getIommuAGAW(IOMMUADDR,
                                              testdevmem_invalid,
                                              CAP_OFFSET,
                                              CAP_REG_BYTE_SIZE,
                                              AGAW_BIT_START) == "agaw"

    def test_createDeviceFromAddr(self):
        testiommuaddr = "0x0"
        devmemloc = os.path.join(self.testdir, "testdevmem")

        iommudev = self.iommucreator.createDeviceFromAddr(devmemloc,
                                                          testiommuaddr,
                                                          "iommu")

        device = src.schemadata.schema.deviceType(name="iommu")

        device.append(src.schemadata.schema.deviceMemoryType(
            name="mmio", caching="UC", physicalAddress="16#0000#",
            size="16#1000#"))

        capabilities = src.schemadata.schema.capabilitiesType()
        capabilities.append(src.schemadata.schema.capabilityType(
            name="iommu"))
        capabilities.append(src.schemadata.schema.capabilityType(
            "39", name="agaw"))
        capabilities.append(src.schemadata.schema.capabilityType(
            "512", name="fr_offset"))
        capabilities.append(src.schemadata.schema.capabilityType(
            "264", name="iotlb_invalidate_offset"))

        device.append(capabilities)

        assert_xb(iommudev, device, "device")
