#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

from . import testpaths
import pytest

import paths
from src import customExceptions, bindings


class TestBindings:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_BINDINGS

    def test_init(self):
        bindingfile = os.path.join(testpaths.PATH_TEST_GEN,
                                   "test_binding_init.py")

        testschema = paths.SCHEMAPATH

        with open(bindingfile, "w") as f:
            f.write("TestBindingFile")

        bindings.init(testschema, bindingfile)
        os.remove(bindingfile)
        bindings.init(testschema, bindingfile)

    def test_createBindings(self):
        testschema = paths.SCHEMAPATH
        testschema_invalid = os.path.join(
            self.testdir, "testschema_invalid.file")
        outpath = testpaths.PATH_TEST_GEN
        outpath_noexists = os.path.join(testpaths.PATH_TEST_GEN, "newBinding")
        # Normal function
        bindings.createBindings(testschema,
                                outpath,
                                "testschemaoutput",
                                paths.PYXB_GEN)

        # Normal function w non-existent dir
        bindings.createBindings(testschema,
                                outpath_noexists,
                                "testschemaoutput",
                                paths.PYXB_GEN)

        # Invalid schema file chosen

        with pytest.raises(customExceptions.PyxbgenInvalidSchema):
            bindings.createBindings(testschema_invalid,
                                    outpath,
                                    "testschemaoutput",
                                    paths.PYXB_GEN)

        # No pyxb detected
        with pytest.raises(OSError):
            bindings.createBindings(testschema,
                                    outpath,
                                    "testschemaoutput",
                                    "invalidcommand")

    def test_copyEnvWithPythonPath(self):
        myenv = os.environ.copy()
        pythonpathstr = ""
        for pythonpath in sys.path:
            pythonpathstr = pythonpathstr + pythonpath + ":"
        myenv["PYTHONPATH"] = pythonpathstr
        assert bindings.copyEnvWithPythonPath() == myenv

    def test_bindingsExist(self):
        testbinding = paths.SCHEMA_BINDING_PATH
        test_notexists = os.path.join(self.testdir, "testschema_notexists.py")
        test_notexists_folder = os.path.join(self.testdir, "notexists/test.py")
        assert bindings.bindingsExist(testbinding)
        assert not bindings.bindingsExist(test_notexists)
        assert not bindings.bindingsExist(test_notexists_folder)
