#   Copyright (C) 2021 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2021 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os

import unittest.mock as mock
import pytest

import src.msrs as msrs
import src.customExceptions as customExceptions

from . import testpaths


class TestMSRs:

    def test_get_msrs_of_interest(self):

        def mock_read_msr(msrpaths, offset, optional, *args):
            return '0x123'

        @mock.patch.object(msrs, "read_msr", mock_read_msr)
        def execute():
            res = msrs.get_msrs_of_interest('unused')
            assert len(res) == len(msrs.MSRs_Of_Interest)
            assert len(res[0]) == 4
            assert res[0]['name'] == 'IA32_FEATURE_CONTROL'
            assert res[len(res) - 1]['name'] == 'IA32_VMX_TRUE_ENTRY_CTLS'

        execute()

    def test_read_msr(self):
        msrpath = [os.path.join(testpaths.PATH_TEST_GEN, "testmsr")]
        with open(msrpath[0], "wb") as f:
            f.write(b"\xab\xcd\xff\xef\x12\x34\x11\x67")
        assert msrs.read_msr(msrpath, 0, 1) == '0xab'
        assert msrs.read_msr(msrpath, 0, 2) == '0xcdab'
        assert msrs.read_msr(
                msrpath, 0, 8) == '0x67113412efffcdab'
        assert msrs.read_msr(
                msrpath, 2, 6) == '0x67113412efff'
        msrpath = [os.path.join(testpaths.PATH_TEST_CREATOR,
                                "testmsr_invalid")]
        with pytest.raises(customExceptions.ForceQuit):
            msrs.read_msr(msrpath, 3, 1)
