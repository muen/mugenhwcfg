#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os

from . import testpaths
import unittest.mock as mock
import pytest

import paths
from src import update
from src import customExceptions


class TestUpdate:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_UPDATE

    def test_update(self):

        def updatePciIds_patch(url, location):
            return True

        def updatePciIds_patchfail(url, location):
            return False

        @mock.patch.object(update, "updatePciIds", updatePciIds_patch)
        def runtest():
            return update.update()

        @mock.patch.object(update, "updatePciIds", updatePciIds_patchfail)
        def runtest_fail():
            return update.update()

        assert runtest() is True
        assert runtest_fail() is False

    def test_updatePciIds(self):
        INVALID_ADDR = "http://test"
        testfile = os.path.join(self.testdir, "test_pciids.ids")
        with pytest.raises(customExceptions.PciIdsInvalidLink):
            update.updatePciIds(INVALID_ADDR, testfile)
        update.updatePciIds(os.path.join(self.testdir, "test_newupdate.ids"),
                            testfile)
