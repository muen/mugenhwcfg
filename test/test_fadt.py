#   Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2017 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil

from . import testpaths
import pytest

import src.fadt as fadt


class TestFADT:

    @pytest.fixture(autouse=True)
    def setUp(self, request):
        self.testdir = os.path.join(testpaths.PATH_TEST_FADT)
        self.outdir = os.path.join(testpaths.PATH_TEST_GEN + "/fadt")

        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

        shutil.copyfile(self.testdir + "/testfadt.dat",
                        self.outdir + "/testfadt.dat")

        ref = os.path.join(self.testdir, "testfadt.dsl")
        with open(ref, "r") as f:
            self.ref_fadt_content = f.read()
            f.close()

        def tearDown():
            fadt._pm1a_cnt = 0
        request.addfinalizer(tearDown)

    def test_get_pm1a_cnt_port(self):
        addr = fadt._get_pm1a_cnt_port(self.ref_fadt_content)
        assert addr == "0x00001804"

    def test_parse_table(self):
        fadt_src = os.path.join(self.testdir, "testfadt.dat")
        fadt.parse_table(fadt_src, self.outdir)
        assert fadt._pm1a_cnt == "0x00001804"

    def test_get_shutdown_port(self):
        fadt._pm1a_cnt = 0x123
        assert fadt.get_shutdown_port() == 0x123
