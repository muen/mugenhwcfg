#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from src import message
from src import customExceptions


class TestMessage:

    def test_reset(self):
        message.reset()
        assert len(message.messagequeue) == 0

    def test_str(self):
        message.reset()
        message.addWarning("Warning1")
        assert message.messagequeue[0].to_str() == "* WARNING *: Warning1"
        message.addError("Error1", False)
        assert message.messagequeue[1].to_str() == "** ERROR **: Error1"

    def test_addMessage(self):
        message.reset()
        message.addWarning("Warning1")
        message.addError("Error1", False)
        message.addError("Error1", False)
        message.addMessage("Message1")

        assert message.messagecount[
            message.WarningMessage
        ] == 1, "WarningMessage count failed"
        assert message.messagecount[
            message.Message
        ] == 1, "Message count failed"
        # Should not keep duplicate messages
        assert message.messagecount[
            message.ErrorMessage
        ] == 1, "ErrorMessage count failed"
        assert len(
            message.messagequeue
        ) == 3, "Message queue duplication handling failed"
        with pytest.raises(
                customExceptions.ForceQuit):
            message.addError("QuitError")

    def test_printMessages(self):
        message.printMessages()
