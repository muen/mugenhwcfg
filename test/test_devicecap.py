#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import io
import os

from . import testpaths
import pytest

import src.devicecap as devicecap
from src import customExceptions


class TestDevicecap:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_DEVICECAP

    def test_translate(self):
        "Tests the translate function"
        assert devicecap.translate("0x10") == "PCI Express"
        with pytest.raises(customExceptions.CapabilityUnknown):
            devicecap.translate("0x0101")

    def test_DevicecapManager(self):
        "Tests the DevicecapManager class"
        devicecapmgr = devicecap.DevicecapManager()

        # -- extractCapabilities function
        devloc = os.path.join(self.testdir, "devices")
        devpaths = []
        devpaths.append(os.path.join(devloc, "dev0"))
        devpaths.append(os.path.join(devloc, "pcibridge0"))
        devpaths = sorted(devpaths)
        devicecapmgr.extractCapabilities(devpaths)

        devicecapmgr.getCapList(os.path.join(devloc, "dev0"), False)
        devicecapmgr.getCapList(os.path.join(devloc, "pcibridge0"))
        devicecapmgr.getCapValue(os.path.join(devloc, "dev0"), "0x09")
        devicecapmgr.getCapValue(os.path.join(devloc, "pcibridge0"), "0x09")

        intInfo = devicecapmgr.getInterruptInfo("nonexistent")
        assert intInfo == devicecap.Interrupt_Info(line=255, pin=0), \
            "Unexpected interrupt info for nonexistent device"
        intInfo = devicecapmgr.getInterruptInfo(os.path.join(devloc, "dev0"))
        assert intInfo.pin == 1, "Device interrupt pin mismatch"
        assert intInfo.line == 11, "Device interrupt line mismatch"

        devpath_noaccess = os.path.join(devloc, "dev1_noaccess")
        with pytest.raises(customExceptions.DeviceCapabilitiesNotRead):
            devicecapmgr.extractCapabilities([devpath_noaccess])

        # -- readCapFile function
        f = io.BytesIO(b'\x00\x01\x04\x03\x0a\x0e\x0c\x0a\x0e\x0c\x0d\x08' +
                       b'\x0f\x00\x0b\x06')
        test_capcode1 = [
            cap.code for cap in devicecapmgr._readCapFile(f, 0x02, 1, 1)]
        test_capcode2 = [
            cap.code for cap in devicecapmgr._readCapFile(f, 0x02, 1, 1, 0, 3)]
        assert test_capcode1 == ["0x0a", "0x0b", "0x0c", "0x0d", "0x0e",
                                 "0x0f"]
        assert test_capcode2 == ["0x0a", "0x0b", "0x0c"]

        # Read out of bounds offset to simulate "no access" - only 64 bytes
        # can be read without being root user
        with pytest.raises(customExceptions.NoAccessToFile):
            devicecapmgr._readCapFile(f, 256, 1, 1)

        f.close()
