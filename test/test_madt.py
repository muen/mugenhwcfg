#   Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import re
import copy

from . import testpaths
import unittest.mock as mock
import pytest
import unittest

import src.acpi as acpi
import src.madt as madt
import src.extractor as extractor


class TestMADT:

    @pytest.fixture(autouse=True)
    def setUp(self, request):
        self.testdir = os.path.join(testpaths.PATH_TEST_MADT)
        self.outdir = os.path.join(testpaths.PATH_TEST_GEN + "/madt")

        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

        shutil.copyfile(self.testdir + "/apic.dat",
                        self.outdir + "/apic.dat")

        madt_src = os.path.join(self.testdir, "apic.dsl")
        self.ref_madt_content = extractor.extractData(madt_src)

        def tearDown():
            madt._iommus = []
        request.addfinalizer(tearDown)

    def test_parse_ioapics(self):
        assert madt._parse_ioapics("") == [], \
            "_parse_ioapics: Found I/O APIC definitions"

        ioapics = madt._parse_ioapics(self.ref_madt_content)
        assert len(ioapics) == 3, "_parse_ioapics: I/O APIC count mismatch"

    def test_parse_table(self):
        madt_src = os.path.join(self.testdir, "apic.dat")
        madt.parse_table(madt_src, self.outdir)

        assert len(madt._ioapics) == 3, "_parse_table: I/O APIC count mismatch"

    def test_get_ioapics(self):
        ref_ioapics = []
        ref_ioapics.append(madt.IO_Apic(id=1, address=0xfec00000, gsi_base=0))
        ref_ioapics.append(madt.IO_Apic(id=2, address=0xfec01000, gsi_base=48))

        madt._ioapics = copy.copy(ref_ioapics)
        assert sorted(madt.get_ioapics()) == sorted(ref_ioapics), \
            "get_ioapics: Returned I/O APICs mismatch"

        ref_ioapics.append(madt.IO_Apic(id=1, address=0xfec02000, gsi_base=24))
        assert sorted(madt.get_ioapics()) != sorted(ref_ioapics), \
            "get_ioapics: Returned I/O APICs match"
