#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os

from . import testpaths
import pytest

import src.util as util


class TestUtil:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_UTIL

    def test_removeListsFromList(self):
        "Tests the removeListsFromList function"
        mainList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        removeList1 = [2, 4]
        removeList2 = [3, 7]
        removeList3 = [3, 6]
        removeList4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        assert util.removeListsFromList(mainList,
                                        removeList1,
                                        removeList2) == [1, 5, 6, 8, 9, 10]
        assert util.removeListsFromList(mainList,
                                        removeList2,
                                        removeList3) == [1, 2, 4, 5, 8, 9, 10]
        assert util.removeListsFromList(mainList, removeList4) == []

    def test_makefolder(self):

        tempfolder = os.path.join(self.testdir, "test_makefolder")
        if os.path.isdir(tempfolder):
            os.rmdir(tempfolder)
        util.makefolder(tempfolder)
        assert os.path.isdir(tempfolder) is True, "makefolder failed"
        os.rmdir(tempfolder)
        os.mkdir(tempfolder)
        util.makefolder(tempfolder)  # see if fails when exists
        os.rmdir(tempfolder)

    def test_find(self):
        "Test find function"
        numlist = [2, 4, 6, 5, 7, 8, 1]

        num = util.find(lambda num: num % 2 == 1, [])
        assert num is None, "Odd number found in empty list"

        oddnum = util.find(lambda num: num % 2 == 1, numlist)
        assert oddnum == 5, "Unexpected odd number found"

    def test_getBit(self):
        "Tests the getBit function"
        assert util.getBit(5, 2) == 1
        assert util.getBit(5, 5) == 0

    def test_getLinks(self):
        "Tests the getLinks function"
        testdir = os.path.join(self.testdir, "test_getFilesInPath")

        def filterexp(filename):
            if filename.startswith("file"):
                return True
            else:
                return False

        testlist = ["doc1", "file0", "file1", "file2", "file3"]
        testfilteredlist = ["file0", "file1", "file2", "file3"]

        # Get the absolute location of symbolic links in path
        testnotfilteredpaths = []
        for filename in testlist:
            filePath = os.path.join(testdir, filename)
            relativeLink = os.readlink(filePath)
            absLink = os.path.join(os.path.dirname(filePath), relativeLink)
            testnotfilteredpaths.append(absLink)

        testfilteredpaths = []
        for filename in testfilteredlist:
            filePath = os.path.join(testdir, filename)
            relativeLink = os.readlink(filePath)
            absLink = os.path.join(os.path.dirname(filePath), relativeLink)
            testfilteredpaths.append(absLink)

        assert util.getLinks(testdir) == testnotfilteredpaths

        assert util.getLinks(testdir, filterexp) == testfilteredpaths

    def test_getSpeedValue(self):
        "Tests the getSpeedValue function"
        validspeeds = ["GHz", "MHz"]
        assert util.getSpeedValue("3.20GHz", validspeeds) == 3200000
        assert util.getSpeedValue("3.22GHz", validspeeds) == 3220000
        assert util.getSpeedValue("800.0MHz", validspeeds) == 800000
        assert util.getSpeedValue("823.789MHz", validspeeds) == 823789
        assert util.getSpeedValue("800kHz", validspeeds) is None
        assert util.getSpeedValue("800kHz", "kHz") == 800
        assert util.getSpeedValue("0GHz", validspeeds) == 0
        assert util.getSpeedValue("GHz", validspeeds) is None
        assert util.getSpeedValue("TenGHz", validspeeds) is None
        assert util.getSpeedValue("2893.430 MHz.", validspeeds) == 2893430

    def test_isHex(self):
        "Tests the isHex function"
        assert util.isHex("0x34Fa") is True
        assert util.isHex("03Fa0x") is False

    def test_stripvalue(self):
        "Tests the stripvalue function"
        assert util.stripvalue("0x5123fa") == "5123fa"
        assert util.stripvalue("100") == "100"

    def test_addPadding(self):
        before = "a\nbb\nccc"
        after = "a   \nbb  \nccc "

        before2 = "a"
        after2 = "a    "

        assert util.addPadding(before, 4) == after
        assert util.addPadding(before2, 5) == after2

    def test_toWord64(self):
        "Tests the toWord64 function"
        with pytest.raises(ValueError):
            util.toWord64("")
        with pytest.raises(ValueError):
            util.toWord64("not_a_hex_number")
        with pytest.raises(ValueError):
            util.toWord64("0xabc123123123131231d")
        with pytest.raises(ValueError):
            util.toWord64("0xabc", blocks=12)
        with pytest.raises(ValueError):
            util.toWord64("0x12302016", blocks=1)

        assert util.toWord64("0x5faFFaD") == "16#05fa_FFaD#"
        assert util.toWord64("0x0") == "16#0000#"
        assert util.toWord64("0x00000016", blocks=3) == "16#0000_0000_0016#"
        assert util.toWord64("0x00000000012",
                             retainzeros=False, blocks=2) == '16#0000_0012#'

    def test_unwrapWord64(self):
        "Tests the unwrapWord64 function"
        assert util.unwrapWord64("16#0009_a000#") == "0x9a000"
        assert util.unwrapWord64("16#0000#") == "0x0"
        assert util.unwrapWord64("0x1234") == "0x1234"
        with pytest.raises(ValueError):
            util.unwrapWord64("15#0009_a000#")

    def test_wrap16(self):
        "Tests the wrap16 function"
        assert util.wrap16("1234") == "16#1234#"
        assert util.wrap16("") == "16##"

    def test_spacesToUnderscores(self):
        "Tests the spacesToUnderscores function"
        assert util.spacesToUnderscores("asd def") == "asd_def"

    def test_sizeOf(self):
        "Tests the sizeOf function"
        assert util.sizeOf("0x0002", "0x0001") == "0x2"
        assert util.sizeOf("0xe000", "0xe07f") == "0x80"
        with pytest.raises(ValueError):
            util.sizeOf("0x1000", 2)

    def test_hexFloor(self):
        "Tests the hexFloor function"
        MINSIZE = "0x1000"
        assert util.hexFloor("0x10", MINSIZE) == MINSIZE
        assert util.hexFloor("0x1000", MINSIZE) == "0x1000"
        assert util.hexFloor("0x10000", MINSIZE) == "0x10000"

    def test_hexRoundToMultiple(self):
        "Tests the hexRoundToMultiple function"
        assert util.hexRoundToMultiple("0x9d800", "0x1000") == "0x9e000"
        assert util.hexRoundToMultiple("0x9e000", "0x1000") == "0x9e000"
        assert util.hexRoundToMultiple(
            "0x9d800", "0x1000", rounddown=True) == "0x9d000"
