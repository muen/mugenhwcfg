#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os

from . import testpaths
import pytest

from src import parseutil
import src.extractor as extractor
from src import customExceptions


class TestParseUtil:

    @pytest.fixture(autouse=True)
    def setUp(self):
        "Setup code"
        self.testdir = testpaths.PATH_TEST_PARSEUTIL

    # -- parseLine_Sep tests
    def test_parseLine_Sep(self):
        "Tests parseLine_Sep normal function"

        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseLine_Sep(data.splitlines()[4],
                                       "testKey4", ":") == "testValue4"
        assert parseutil.parseLine_Sep(data.splitlines()[0],
                                       "testKey") == "testValue"
        assert parseutil.parseLine_Sep(data.splitlines()[0],
                                       "testKey",
                                       [":", ",", ""]) == "testValue"

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseLine_Sep(
            data.splitlines()[8], "cache size", ":") == "8192 KB"
        assert parseutil.parseLine_Sep(
            data.splitlines()[4],
            "model name", ":") == "Intel(R) Xeon(R) CPU E31230 @ 3.20GHz"

    def test_parseLine_Sep_keyDoesNotExist(self):
        "Tests parseLine_Sep with nonexistent key"

        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        with pytest.raises(customExceptions.KeyNotFound):
            parseutil.parseLine_Sep(data.splitlines()[0], "testKey2")
        with pytest.raises(customExceptions.KeyNotFound):
            parseutil.parseLine_Sep(data.splitlines()[0], "testKey12")

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        with pytest.raises(customExceptions.KeyNotFound):
            parseutil.parseLine_Sep(data.splitlines()[0], "numprocessors")

    def test_parseLine_Sep_valueDoesNotExist(self):
        "Tests parseLine_Sep with nonexistent value"

        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseLine_Sep(data.splitlines()[2],
                                       "testKey3") == "NO_VALUE"
        assert parseutil.parseLine_Sep(data.splitlines()[5],
                                       "testKey5", ":") == "NO_VALUE"

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseLine_Sep(data.splitlines()[24],
                                       "power management",
                                       ":") == "NO_VALUE"

    def test_parseLine_Sep_sepDoesNotExist(self):
        "Tests parseLine_Sep when separator cannot be found"

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(
            data.splitlines()[5], "stepping", ",") == "NO_VALUE"

        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(data.splitlines()[0],
                                       "testKey", ":") == "NO_VALUE"

    # -- parseData_Sep tests
    def test_parseData_Sep(self):
        "Tests parseData_Sep normal function"
        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(data, "testKey4", ":") == "testValue4"
        assert parseutil.parseData_Sep(
            data, "testKey2") == "testValue2 testValue2.1 testValue2.2"

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(data, "cpu cores", ":") == "4"

    def test_parseData_Sep_keyNotFound(self):
        "Tests parseData_Sep with inexistent key"
        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        with pytest.raises(customExceptions.KeyNotFound):
            parseutil.parseData_Sep(data, "testKeyNotExists")

        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        with pytest.raises(customExceptions.KeyNotFound):
            parseutil.parseData_Sep(data, "testKeyNotExists")

    def test_parseData_Sep_valueDoesNotExist(self):
        "Tests parseData_Sep to obtain value that does not exist"
        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(
            data, "power management", ":") == "NO_VALUE"

    def test_parseData_Sep_sepDoesNotExist(self):
        "Tests parseData_Sep when separator cannot be found"
        loc = self.testdir + "testCpuInfo.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(data, "stepping", ",") == "NO_VALUE"

        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.parseData_Sep(data, "testKey", ":") == "NO_VALUE"

    # -- findLines tests
        "Tests findLines function"
        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.findLines(data, "testValue4")[
            0] == "testKey4 : testValue4"

    # -- count tests
    def test_count(self):
        "Tests count with normal function"
        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.count(data, "testCount") == 6, \
            "Value obtained from file " + loc + " is incorrect"
        assert parseutil.count(data, "testCount3") == 2, \
            "Value obtained from file " + loc + " is incorrect"

    def test_count_keyNotFound(self):
        "Tests count with nonexistent key"
        loc = self.testdir + "testParseUtil.txt"
        data = extractor.extractData(loc)
        assert parseutil.count(data, "testCount4") == 0, \
            "Value obtained from file " + loc + " is incorrect"

    # -- PciIdsParser tests
    def test_PciIdsParser(self):
        "Tests the PciIdsParser class"
        pciIdsLoc = testpaths.PATH_PCIIDS
        pciIdsLocMultiple = testpaths.PATH_TEST_PARSEUTIL + \
            "testpciids_multiple"
        pciIdsLocInit = testpaths.PATH_TEST_PARSEUTIL + "testpciids_init"
        parser = parseutil.PciIdsParser(pciIdsLoc)

        # isValidVendorCode function
        assert parser.isValidVendorCode("0000") is True
        assert parser.isValidVendorCode("a0102") is False
        assert parser.isValidVendorCode("#") is False

        # isValidDeviceCode function
        assert parser.isValidDeviceCode("0000") is True
        assert parser.isValidDeviceCode("12") is False
        assert parser.isValidDeviceCode("#") is False
        assert parser.isValidDeviceCode("____") is False

        # isValidClassCode function
        assert parser.isValidClassCode("02") is True
        assert parser.isValidClassCode("0301") is False
        assert parser.isValidClassCode("#") is False

        # isValidSubclassCode function
        assert parser.isValidSubclassCode("01") is True
        assert parser.isValidSubclassCode("a12") is False
        assert parser.isValidSubclassCode("#") is False

        # isVendor function
        assert parser.isVendor("0a12  Vendor1") is True
        assert parser.isVendor("0a12  Vendor1, Inc.") is True
        assert parser.isVendor("	0a12  Vendor2") is False

        # isDevice function
        assert parser.isDevice("	0101  Device1") is True
        assert parser.isDevice("      0203  Device1_spacenottab") is False
        assert parser.isDevice("		0232  Device1") is False

        # isClass function
        assert parser.isClass("C 01  Class1") is True
        assert parser.isClass("0a12 Vendor1") is False

        # isSubclass function
        assert parser.isSubclass("	02  Subclass1") is True
        assert parser.isSubclass("		02  Subsubclass") is False

        # init function
        parser_init = parseutil.PciIdsParser(pciIdsLocInit)
        assert len(parser_init.vendorData) == 6
        assert len(parser_init.deviceData) == 3
        assert len(parser_init.classData) == 10

        with pytest.raises(
                customExceptions.PciIdsMultipleEntries):
            parseutil.PciIdsParser(pciIdsLocMultiple)

        # getVendorName function
        assert parser.getVendorName("0x0e11") == "Compaq Computer Corporation"
        with pytest.raises(
                customExceptions.PciIdsFailedSearch):
            parser.getVendorName("0x0400")

        # getDeviceName function
        assert parser.getDeviceName(
            "0x0675", "0x1700") == "IS64PH ISDN Adapter"
        assert parser.getDeviceName(
            "0x0675", "0x1704") == "ISDN Adapter (PCI Bus, D, C)"
        assert parser.getDeviceName(
            "0x8086",
            "0x0108") == "Xeon E3-1200 Processor Family DRAM Controller"
        with pytest.raises(
                customExceptions.PciIdsFailedSearch):
            parser.getDeviceName("0x0675", "0x2000")

        # getClassName function
        assert parser.getClassName("0x0604") == "PCI bridge"
        assert parser.getClassName("0x0c06") == "InfiniBand"
        assert parser.getClassName("0x0608") == "RACEway bridge"
        assert parser.getClassName("0x0600") == "Host bridge"
        with pytest.raises(
                customExceptions.PciIdsSubclassNotFound):
            parser.getClassName("0x0685")
        with pytest.raises(
                customExceptions.PciIdsFailedSearch):
            parser.getClassName("0x1400")


class TestDMIParser:

    @pytest.fixture(autouse=True)
    def setUp(self):
        "Setup code"
        self.testdir = os.path.join(testpaths.PATH_TEST_PARSEUTIL)
        self.dmiparser = parseutil.DMIParser(os.path.join(self.testdir, "dmi"))

    def test_getData(self):
        assert self.dmiparser.getData("bios_vendor") == "test_bios_vendor"
        assert self.dmiparser.getData("bios_version") == ""
        with pytest.raises(KeyError):
            self.dmiparser.getData("incorrectdatakey")
        with pytest.raises(IOError):
            self.dmiparser.getData("product_vendor")
