# File containing paths for tests
import os
CURRENTDIR = os.path.dirname(__file__)
DATADIR = CURRENTDIR + "/../data/"

PATH_TEST_EXTRACTOR = DATADIR + "/extractor/"
PATH_TEST_PARSEUTIL = DATADIR + "/parseutil/"
PATH_TEST_SCHEMADATA = CURRENTDIR + "/schemadata/"
PATH_TEST_CREATOR = DATADIR + "/creator/"
PATH_TEST_UTIL = DATADIR + "/util/"
PATH_TEST_UPDATE = DATADIR + "/update/"
PATH_TEST_DEVICECAP = DATADIR + "/devicecap/"
PATH_TEST_BINDINGS = DATADIR + "/bindings/"
PATH_TEST_OUTPUT = CURRENTDIR + "/output/"
PATH_TEST_DMAR = DATADIR + "/dmar/"
PATH_TEST_DSDT = DATADIR + "/dsdt/"
PATH_TEST_FADT = DATADIR + "/fadt/"
PATH_TEST_MADT = DATADIR + "/madt/"
PATH_TEST_MCFG = DATADIR + "/mcfg/"

PATH_PCIIDS = os.path.join(DATADIR, "pci.ids")
PATH_SERIALS = os.path.join(PATH_TEST_CREATOR,
                            "devicescreator/devices_testserial")
PATH_DEVICELINKS = os.path.join(PATH_TEST_CREATOR,
                                "devicescreator/devices_test_links")
PATH_IOMMUGRP = os.path.join(PATH_TEST_CREATOR, "iommu_groups")
PATH_TEST_GEN = CURRENTDIR + "/gen/"
