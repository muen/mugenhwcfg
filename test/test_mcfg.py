#   Copyright (C) 2019 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2019 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import copy

from . import testpaths
import pytest

import src.mcfg as mcfg
import src.extractor as extractor
from src import message


class TestMCFG:

    @pytest.fixture(autouse=True)
    def setUp(self, request):
        self.testdir = os.path.join(testpaths.PATH_TEST_MCFG)
        self.outdir = os.path.join(testpaths.PATH_TEST_GEN + "/mcfg")

        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

        shutil.copyfile(self.testdir + "/mcfg.dat",
                        self.outdir + "/mcfg.dat")

        mcfg_src = os.path.join(self.testdir, "mcfg.dsl")
        self.ref_mcfg_content = extractor.extractData(mcfg_src)

        def tearDown():
            mcfg._entries = []
        request.addfinalizer(tearDown)

    def test_parse_mmconfs(self):
        assert mcfg._parse_mmconfs("") == [], \
            "_parse_mmconfs: Found MMCONF definitions"

        mmconfs = mcfg._parse_mmconfs(self.ref_mcfg_content)
        assert len(mmconfs) == 1, "_parse_mmconfs: MMCONF entry count mismatch"

    def test_parse_table(self):
        mcfg.parse_table(["nonexistent1", "nonexistent2"], self.outdir)
        assert len(mcfg._entries) == 0, "_parse_table: found unexpected data"
        message.printMessages()

        ref_entries = []
        ref_entries.append(mcfg.MCFG_Entry(address=0xe0000000, segment=0x0,
                                           start=0x0, end=0xff))
        mcfg_src = ["/nonexistent/mcfg.dat",
                    os.path.join(self.testdir, "mcfg.dat")]
        mcfg.parse_table(mcfg_src, self.outdir)

        assert len(mcfg._entries) == 1, \
            "_parse_table: MMCONF entry count mismatch"
        assert sorted(mcfg._entries) == sorted(ref_entries), \
            "_parse_table: MMCONF entries mismatch"

    def test_get_entries(self):
        ref_entries = []
        ref_entries.append(mcfg.MCFG_Entry(address=0xe0000000, segment=0x0,
                                           start=0x0, end=0x3f))

        mcfg._entries = copy.copy(ref_entries)
        assert sorted(mcfg.get_entries()) == sorted(ref_entries), \
            "get_entries: Returned MMCONF mismatch"

        ref_entries.append(mcfg.MCFG_Entry(address=0xf8000000, segment=0x0,
                                           start=0x0, end=0xff))
        assert sorted(mcfg.get_entries()) != sorted(ref_entries), \
            "get_entries: Returned MMCONF match"
