#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import re

from . import testpaths
import unittest.mock as mock
import pytest

import src.acpi as acpi
import src.dmar as dmar


class TestDMAR:

    @pytest.fixture(autouse=True)
    def setUp(self, request):
        self.testdir = os.path.join(testpaths.PATH_TEST_DMAR)
        self.outdir = os.path.join(testpaths.PATH_TEST_GEN + "/dmar")

        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

        shutil.copyfile(self.testdir + "/testdmar.dat",
                        self.outdir + "/testdmar.dat")

        dmar_ref = os.path.join(self.testdir, "testdmar.ref.dsl")
        with open(dmar_ref, "r") as f:
            self.ref_dmar_content = f.read()
            f.close()

        def tearDown():
            dmar._iommu_addrs = []
            dmar._rmrrs = []
            dmar._devices = []
            dmar._ioapics = []
        request.addfinalizer(tearDown)

    def test_run_iasl(self):
        acpi._run_iasl(self.outdir + "/testdmar")
        assert os.path.isfile(self.outdir + "/testdmar.dsl"), \
            "_run_iasl: error disassembling DMAR table with iasl"

    def test_decompile_table(self):
        dmar_content = re.compile(r'\[.*', re.MULTILINE)
        dmar_src = os.path.join(self.testdir, "testdmar.dat")
        dmar_ref = os.path.join(self.testdir, "testdmar.ref.dsl")

        data = acpi.decompile_table(dmar_src, self.outdir, "err")
        assert dmar_content.match(data) == dmar_content.match(
            self.ref_dmar_content), \
            "acpi.decompile_table: Table content mismatch"

        assert acpi.decompile_table("nonexistent", self.outdir, "err") == "", \
            "acpi.decompile_table: Got content for non-existent path"

        assert acpi.decompile_table(dmar_src, "invalid/path", "err") == "", \
            "acpi.decompile_table: Got content for invalid path"

        def run_iasl_oserror(prefix):
            raise OSError

        with mock.patch('src.acpi._run_iasl', run_iasl_oserror):
            assert acpi.decompile_table(dmar_src, self.outdir, "err") == "", \
                "acpi.decompile_table: Got content with iasl error"

    def test_parse_ioapic_devices(self):
        assert dmar._parse_ioapic_devices("") == [], \
            "_parse_ioapic_devices: Got addresses"

        ioapics = dmar._parse_ioapic_devices(self.ref_dmar_content)
        assert len(ioapics) == 1, \
            "_parse_ioapic_devices: I/O APIC count mismatch"
        assert ioapics[0].bus == 0xf0, \
            "_parse_ioapic_devices: bus number mismatch"
        assert ioapics[0].device == 0x1f, \
            "_parse_ioapic_devices: device number mismatch"
        assert ioapics[0].function == 0, \
            "_parse_ioapic_devices: function number mismatch"

    def test_parse_hardware_unit_definitions(self):
        assert dmar._parse_hardware_unit_definitions("") == [], \
            "_parse_hardware_unit_definitions: Got addresses"

        addrs = dmar._parse_hardware_unit_definitions(self.ref_dmar_content)
        for i, addr in enumerate(addrs):
            assert addr == "0x00000000fed91000", \
                "_parse_hardware_unit_definitions: address %s " \
                "mismatch" % str(i)

    def test_parse_rmrrs(self):
        regions, devices = dmar._parse_rmrrs("")
        assert regions == [], "_parse_rmrrs: Got regions for empty table"
        assert devices == [], "_parse_rmrrs: Got devices for empty table"

        regions, devices = dmar._parse_rmrrs(self.ref_dmar_content)
        assert len(regions) == 1, "_parse_rmrrs: region count mismatch"
        assert regions[0].name == "rmrr1", "_parse_rmrrs: region name mismatch"
        assert regions[0].start == "0x00000000bf59e000", \
            "_parse_rmrrs: region start address mismatch"
        assert regions[0].end == "0x00000000bf5acfff", \
            "_parse_rmrrs: region end address mismatch"

        assert len(devices) == 2, "_parse_rmrrs: devices count mismatch"
        assert devices[0].bus == 0, "_parse_rmrrs: bus number mismatch (1)"
        assert devices[0].device == 0x1d, \
            "_parse_rmrrs: device number mismatch (1)"
        assert devices[0].function == 0, \
            "_parse_rmrrs: function number mismatch (1)"
        assert devices[0].rmrr_name == "rmrr1", \
            "_parse_rmrrs: rmrr name mismatch (1)"
        assert devices[1].bus == 0, "_parse_rmrrs: bus number mismatch (2)"
        assert devices[1].device == 0x1a, \
            "_parse_rmrrs: device number mismatch (2)"
        assert devices[1].function == 0, \
            "_parse_rmrrs: function number mismatch (2)"
        assert devices[1].rmrr_name == "rmrr1", \
            "_parse_rmrrs: rmrr name mismatch (2)"

    def test_parse_table(self):
        dmar_src = os.path.join(self.testdir, "testdmar.dat")
        dmar.parse_table(dmar_src, self.outdir)

        assert len(dmar._iommu_addrs) == 1, \
            "_parse_table: IOMMU address count mismatch"
        assert len(dmar._rmrrs) == 1, \
            "_parse_table: RMRR count mismatch"
        assert len(dmar._devices) == 2, \
            "_parse_table: PCI devices count mismatch"
        assert len(dmar._ioapics) == 1, \
            "_parse_table: I/O APIC count mismatch"

    def test_get_ioapic_devices(self):
        assert dmar.get_ioapic_devices() == dmar._ioapics, \
            "_get_iommu_addrs: I/O APIC information mismatch"

    def test_get_iommu_addrs(self):
        assert dmar.get_iommu_addrs() == dmar._iommu_addrs, \
            "_get_iommu_addrs: IOMMU addresses mismatch"

    def test_get_reserved_mem_regions(self):
        assert dmar.get_reserved_mem_regions() == dmar._rmrrs, \
            "_get_reserved_mem_regions: RMRRs mismatch"

    def test_get_referenced_rmrr(self):
        dmar._devices.append(dmar.PCI_Dev(bus=0x0, device=0x1c,
                             function=0x0, rmrr_name="rmrr5"))
        dmar._devices.append(dmar.PCI_Dev(bus=0x0, device=0x12,
                             function=0x1, rmrr_name="rmrr2"))

        assert dmar.get_referenced_rmrr(0, 0, 0) is None, \
            "get_referenced_rmrr: Got RMRR for 00:00.0"
        assert dmar.get_referenced_rmrr(0, 0x1f, 2) is None, \
            "get_referenced_rmrr: Got RMRR for 00:1f.2"
        assert dmar.get_referenced_rmrr(0, 0x1c, 1) is None, \
            "get_referenced_rmrr: Got RMRR for 00:1c.1"
        assert dmar.get_referenced_rmrr(0, 0x1c, 0) == "rmrr5", \
            "get_referenced_rmrr: RMRR for 00:1c.0 mismatch"
        assert dmar.get_referenced_rmrr(0, 0x12, 1) == "rmrr2", \
            "get_referenced_rmrr: RMRR for 00:12.1 mismatch"
