#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import os

from . import testpaths
import unittest.mock as mock
import pytest
import pyxb

import src.output as output
import src.message as message
import src.schemadata
from src import customExceptions
from src import parseutil


class TestOutput:

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.testdir = testpaths.PATH_TEST_OUTPUT

    def test_produceLine(self):
        info = "info"
        result = "   info\n"
        SPACES = 3
        assert output.produceLine(info, SPACES) == result

        info = "info1\ninfo2"
        result = "   info1\n   info2\n"
        assert output.produceLine(info, SPACES) == result

        emptyinfo = ""
        result = ""
        assert output.produceLine(emptyinfo, SPACES) == result

    def test_output(self):
        xml = "<test/>"
        testfile = os.path.join(testpaths.PATH_TEST_GEN, "output_xml.xml")
        output.output(xml, testfile)
        with open(testfile, "r") as f:
            assert f.read() == xml

    def test_formatXML(self):
        xml = ("""<capabilities><capability name="iommu"/>""" +
               """<capability name="agaw">39</capability></capabilities>""")
        formatted = ("""<?xml version='1.0' encoding='utf-8'?>\n"""
                     """<capabilities>\n"""
                     """  <capability name="iommu"/>\n"""
                     """  <capability name="agaw">39</capability>\n"""
                     """</capabilities>\n""")

        def mock_importmodule_error(*args):
            raise ImportError

        assert output.formatXML(xml, "utf-8")[0] == formatted
        assert output.formatXML(xml, "utf-8")[1] is True

        with mock.patch('importlib.import_module',
                        side_effect=mock_importmodule_error):
            assert output.formatXML(xml, "utf-8")[1] is False

    def test_produceHeader(self):
        testresult = ("""<!-- =====\n"""
                      """     headerinfo\n"""
                      """     ===== -->\n""")

        def produceinfo():
            return "     headerinfo\n", True

        BORDER_WIDTH = 5
        assert output.produceHeader(BORDER_WIDTH, produceinfo) == testresult

    def test_genXML(self):
        hardware = src.schemadata.schema.hardwareType()

        testresult = ("""<?xml version='1.0' encoding='utf-8'?>\n"""
                      """header\n\n"""
                      """<hardware/>\n""")

        def mock_produceHeader(*args):
            return "header"

        @mock.patch.object(output, "produceHeader", mock_produceHeader)
        def test():
            pyxb.RequireValidWhenGenerating(False)
            result = output.genXML(hardware, "utf-8")
            pyxb.RequireValidWhenGenerating(True)
            return result

        assert test() == testresult

    def test_produce_toolver(self):

        def mockcommand(*args):
            return "version"

        def mockcommand_fail(*args):
            raise customExceptions.FailedOutputCommand()

        @mock.patch.object(output, "_runCommand", mockcommand)
        def test_success():
            return output.produce_toolver()

        @mock.patch.object(output, "_runCommand", mockcommand_fail)
        def test_fail():
            return output.produce_toolver()

        successline = ("""     Generated with mugenhwcfg (commit version)\n""")

        assert test_success()[0] == successline

        assert test_success()[1] is True

        assert test_fail()[1] is False

    def test_produce_linuxver(self):

        def mockcommand(*args):
            return "version"

        def mockcommand_fail(*args):
            raise customExceptions.FailedOutputCommand()

        @mock.patch.object(output, "_runCommand", mockcommand)
        def test_success():
            return output.produce_linuxver()

        @mock.patch.object(output, "_runCommand", mockcommand_fail)
        def test_fail():
            return output.produce_linuxver()

        successline = ("""     Linux kernel version: version\n""")

        assert test_success()[0] == successline

        assert test_success()[1] is True

        assert test_fail()[1] is False

    def test_produce_distver(self):

        def mockcommand(*args):
            return "Description: 1.0"

        def mockcommand_nokey(*args):
            return "Version: 1.0"

        def mockcommand_fail(*args):
            raise customExceptions.FailedOutputCommand()

        @mock.patch.object(output, "_runCommand", mockcommand)
        def test_success():
            return output.produce_distver()

        @mock.patch.object(output, "_runCommand", mockcommand_nokey)
        def test_nokey():
            return output.produce_distver()

        @mock.patch.object(output, "_runCommand", mockcommand_fail)
        def test_fail():
            return output.produce_distver()

        successline = ("""     Distribution: 1.0\n""")

        assert test_success()[0] == successline

        assert test_success()[1] is True

        assert test_nokey()[1] is False

        assert test_fail()[1] is False

    def test_produce_boardinfo(self):

        successline = ("""     Board information:\n"""
                       """       Vendor: test\n"""
                       """       Name: test\n"""
                       """       Version: test\n""")

        def mock_getData(*args):
            return "test"

        def mock_getData_except(*args):
            raise IOError

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData)
        def test_success():
            return output.produce_boardinfo()

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData_except)
        def test_fail():
            return output.produce_boardinfo()

        assert test_success()[0] == successline
        assert test_success()[1] is True
        assert test_fail()[1] is False

    def test_produce_biosinfo(self):

        successline = ("""     BIOS information:\n"""
                       """       Vendor: test\n"""
                       """       Version: test\n"""
                       """       Date: test\n""")

        def mock_getData(*args):
            return "test"

        def mock_getData_except(*args):
            raise IOError

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData)
        def test_success():
            return output.produce_biosinfo()

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData_except)
        def test_fail():
            return output.produce_biosinfo()

        assert test_success()[0] == successline
        assert test_success()[1] is True
        assert test_fail()[1] is False

    def test_produce_productinfo(self):

        successline = ("""     Product information:\n"""
                       """       Vendor: test\n"""
                       """       Name: test\n"""
                       """       Product Version: test\n""")

        def mock_getData(*args):
            return "test"

        def mock_getData_except(*args):
            raise IOError

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData)
        def test_success():
            return output.produce_productinfo()

        @mock.patch.object(parseutil.DMIParser, "getData", mock_getData_except)
        def test_fail():
            return output.produce_productinfo()

        assert test_success()[0] == successline
        assert test_success()[1] is True
        assert test_fail()[1] is False

    def test_produce_log(self):

        successline = ("""     Log:\n       * WARNING *: Warning1\n""")

        mock_queue = []
        mock_queue.append(message.WarningMessage("Warning1"))

        @mock.patch.object(message, "messagequeue", mock_queue)
        def test_success():
            return output.produce_log()

        assert test_success() == (successline, True)

        mock_queue = []

        @mock.patch.object(message, "messagequeue", mock_queue)
        def test_empty():
            return output.produce_log()
        assert test_empty() == ("", True)

    def test_runCommand(self):

        def command_fail(*args, **kwargs):
            raise subprocess.CalledProcessError(-1, -1, -1)

        @mock.patch.object(subprocess, "check_call", command_fail)
        def test_fail():
            return output._runCommand("commandline", "error")

        assert output._runCommand("echo success", "err") == "success"
        with pytest.raises(customExceptions.FailedOutputCommand):
            test_fail()
