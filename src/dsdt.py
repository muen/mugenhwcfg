#   Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module that handles extraction of data from the DSDT ACPI table

from collections import namedtuple

import re

from . import acpi
from . import extractor
from . import util

prt_entry = re.compile(r'(^\s*\[Package\] Contains 4 Elements\:\s'
                       r'((?!(\s*\[Package\]))(\s|.))*)',
                       re.MULTILINE)
prt_int = re.compile(r'^\s*\[Integer\] = ([0-9A-F]*)', re.MULTILINE)

PRT_Mapping = namedtuple("Mapping", "address, pin, source, source_index")

_prt = []


def get_dev_nr(entry):
    """
    Return device number based on address of given PRT entry.
    """
    return entry.address >> 16


def parse_pci_routing_table(data):
    """
    Parse DSDT PCI Routing Table and store the extracted mappings.
    """
    for entry in prt_entry.finditer(data):
        items = re.findall(prt_int, entry.group(0))
        _prt.append(PRT_Mapping(*[int(x, 16) for x in items]))


def load_prt(path, temp_path):
    """
    Load and parse _PRT from DSDT file specified by path.
    """
    CMD_GET_PRT = r'Execute _PIC 1;Execute \_SB_.PCI0._PRT'
    raw_prt = acpi.execute_aml(path, CMD_GET_PRT, temp_path,
                               "Could not obtain PCI Routing Table (_PRT) from"
                               " DSDT")
    parse_pci_routing_table(raw_prt)


def get_global_system_interrupt(device, irq_pin):
    """
    Return the global system interrupt number for the PCI device with specified
    device number and IRQ pin (INTA#: 0, INTB#: 1 ...).

    NOTE: ACPI and PCI config space encode IRQ pins differently. The PCI
          specification section 6.2.4 says that INTA# has value 0, INTB# 1...
    """
    elem = util.find(lambda entry: entry.pin == irq_pin and get_dev_nr(entry)
                     == device, _prt)
    if elem:
        return elem.source_index
    else:
        return None
