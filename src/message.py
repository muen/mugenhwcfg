#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module to contain functions to handle messages
from . import customExceptions
import textwrap

textWrapper = textwrap.TextWrapper(
    break_long_words=False, replace_whitespace=False, width=80)


class Message():
    shortname = "message(s)"
    "Class that describes messages displayed at end of program"

    def __init__(self, msg):
        self.msg = msg

    def to_str(self):
        return textWrapper.fill(self.msg)


class WarningMessage(Message):
    shortname = "warning(s)"

    def __init__(self, msg):
        self.msg = msg

    def to_str(self):
        return textWrapper.fill("* WARNING *: %s" % self.msg)


class ErrorMessage(Message):
    shortname = "error(s)"

    def __init__(self, msg):
        self.msg = msg

    def to_str(self):
        return textWrapper.fill("** ERROR **: %s" % self.msg)


def addToCount(object):
    base_class = type(object)
    if base_class not in messagecount:
        messagecount[base_class] = 1
    else:
        messagecount[base_class] += 1


def printMessages():
    for message in messagequeue:
        print(message.to_str())
        print("")


def add(Message):
    for message in messagequeue:
        if Message.msg == message.msg:
            return

    messagequeue.append(Message)
    addToCount(Message)


def addWarning(msg):
    add(WarningMessage(msg))


def addMessage(msg):
    add(Message(msg))


def addError(msg, forcequit=True):
    """
    Adds Error Message class to messagequeue; Sets flag to end program if
    "forcequit is True"""
    add(ErrorMessage(msg))
    if forcequit:
        forceQuit()


def forceQuit():
    raise customExceptions.ForceQuit()


def reset():
    globals()["messagequeue"] = []
    globals()["messagecount"] = {}


messagecount = {}  # Message, count
messagequeue = []
