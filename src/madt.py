#   Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module that handles extraction of data from the MADT (APIC) ACPI table

from collections import namedtuple

import re

from . import acpi

ioapic_table = re.compile(r'^.*Subtable Type : 01 \[I/O APIC\]\s'
                          r'((?!(\s.*Subtable Type))(\s|.))*', re.MULTILINE)
ioapic_addr = re.compile(r'.*Address : '
                         r'(?P<address>[0-9a-fA-F]+)')
ioapic_gsi_base = re.compile(r'.*Interrupt : (?P<gsi_base>[0-9a-fA-F]+)')
ioapic_id = re.compile(r'.*I/O Apic ID : (?P<id>[0-9a-fA-F]+)')

IO_Apic = namedtuple("IO_Apic", "id, address, gsi_base")

_ioapics = []


def _parse_ioapics(data):
    """
    Parse I/O APIC definition subtables and store the extracted address and GSI
    base number.
    """
    devices = []
    for i, entry in enumerate(ioapic_table.finditer(data), 1):
        match = ioapic_addr.search(entry.group(0))
        if match:
            address = "0x" + match.group("address").lower()
        match = ioapic_gsi_base.search(entry.group(0))
        if match:
            gsi_base = "0x" + match.group("gsi_base").lower()
        match = ioapic_id.search(entry.group(0))
        if match:
            id = "0x" + match.group("id").lower()

        devices.append(IO_Apic(address=int(address, 16),
                       gsi_base=int(gsi_base, 16),
                       id=int(id, 16)))

    return devices


def parse_table(path, temp_path):
    """
    Parses a MADT/APIC table
    """
    global _ioapics

    print("> Decompiling MADT/APIC table")
    data = acpi.decompile_table(path, temp_path,
                                "Could not obtain MADT/APIC information; "
                                "I/O APIC device information not found.")

    print("> Parsing I/O APIC definitions")
    _ioapics = _parse_ioapics(data)


def get_ioapics():
    """
    Return I/O APIC information extracted from MADT/APIC table
    """
    return _ioapics
