#   Copyright (C) 2021 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2021 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.

from . import extractor
from . import message
from . import util

MSRs_Of_Interest = (
    {'address': '16#0000_003a#', 'name': 'IA32_FEATURE_CONTROL'},
    {'address': '16#0000_010a#', 'name': 'IA32_ARCH_CAPABILITIES',
     'optional': True},
    {'address': '16#0000_01a0#', 'name': 'IA32_MISC_ENABLE'},
    {'address': '16#0000_0480#', 'name': 'IA32_VMX_BASIC'},
    {'address': '16#0000_0481#', 'name': 'IA32_VMX_PINBASED_CTLS'},
    {'address': '16#0000_0482#', 'name': 'IA32_VMX_PROCBASED_CTLS'},
    {'address': '16#0000_0483#', 'name': 'IA32_VMX_EXIT_CTLS'},
    {'address': '16#0000_0484#', 'name': 'IA32_VMX_ENTRY_CTLS'},
    {'address': '16#0000_0485#', 'name': 'IA32_VMX_MISC'},
    {'address': '16#0000_0486#', 'name': 'IA32_VMX_CR0_FIXED0'},
    {'address': '16#0000_0487#', 'name': 'IA32_VMX_CR0_FIXED1'},
    {'address': '16#0000_0488#', 'name': 'IA32_VMX_CR4_FIXED0'},
    {'address': '16#0000_0489#', 'name': 'IA32_VMX_CR4_FIXED1'},
    {'address': '16#0000_048b#', 'name': 'IA32_VMX_PROCBASED_CTLS2'},
    {'address': '16#0000_048c#', 'name': 'IA32_VMX_EPT_VPID_CAP'},
    {'address': '16#0000_048d#', 'name': 'IA32_VMX_TRUE_PINBASED_CTLS'},
    {'address': '16#0000_048e#', 'name': 'IA32_VMX_TRUE_PROCBASED_CTLS'},
    {'address': '16#0000_048f#', 'name': 'IA32_VMX_TRUE_EXIT_CTLS'},
    {'address': '16#0000_0490#', 'name': 'IA32_VMX_TRUE_ENTRY_CTLS'}
)


def get_msrs_of_interest(msrpaths):
    """Return register values for MSRs of interest. If an MSR is marked as
    ''optional', and it is not available, it is skipped.

    :param msrpaths: Path to MSR file(s)
    """
    msrs = MSRs_Of_Interest
    [msr.setdefault('optional') for msr in msrs]
    result = []

    for msr in msrs:
        val = read_msr(msrpaths=msrpaths,
                       offset=int(util.unwrapWord64(msr['address']), 16),
                       optional=msr['optional'])
        if val:
            msr.update({'regval': util.toWord64(value=val, blocks=4)})
            result.append(msr)

    return result


def read_msr(msrpaths, offset, width=8, optional=False):
    """Read value from MSR given as offset. If 'optional' is True, no error
    message is added to the message buffer.

    :param msrpaths: Path to MSR file(s)
    :param offset: MSR address
    :param width: Register value width
    :param optional: Whether the MSR is optional
    """
    value = ''

    msr_found = False
    for path in msrpaths:
        try:
            # Try to find MSR file
            value = extractor.extractBinaryData(path, offset, width)
        except IOError:
            pass
        else:
            msr_found = True
            break
    if not msr_found and not optional:
        errormsg = "Unable to access MSRs at offset " \
            + hex(offset) + ", tried path(s):\n"
        for p in msrpaths:
            errormsg += p + '\n'

        errormsg += ("Try 'modprobe msr' to load kernel module and try "
                     "again.")
        message.addError(errormsg)

    return value
