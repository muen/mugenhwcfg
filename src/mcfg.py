#   Copyright (C) 2019 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2019 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module that handles extraction of data from the MCFG ACPI table

from collections import namedtuple

import os
import re

from . import acpi, message

mcfg_table = re.compile(r'^.*Signature : \"MCFG\"\s'
                        r'((?!(\s.*Signature))(\s|.))*', re.MULTILINE)
mcfg_entry = re.compile(r'.*Base Address : (?P<base_addr>[0-9a-fA-F]+)\s+'
                        r'.*Segment Group Number : (?P<seg>[0-9a-fA-F]{4})\s+'
                        r'.*Start Bus Number : (?P<start>[0-9a-fA-F]{2})\s+'
                        r'.*End Bus Number :[\s\[]*(?P<end>[0-9a-fA-F]{2})')

MCFG_Entry = namedtuple("MMCONF", "address, segment, start, end")

_entries = []


def _parse_mmconfs(data):
    """
    Parse MMCONF definition entries and store the extracted address, segment
    and bus start/end numbers.
    """
    entries = []
    for i, entry in enumerate(mcfg_table.finditer(data), 1):
        match = mcfg_entry.search(entry.group(0))
        if match:
            base_address = "0x" + match.group("base_addr").lower()
            segment_nr = "0x" + match.group("seg").lower()
            start_bus = "0x" + match.group("start").lower()
            end_bus = "0x" + match.group("end").lower()

            entries.append(MCFG_Entry(address=int(base_address, 16),
                           segment=int(segment_nr, 16),
                           start=int(start_bus, 16),
                           end=int(end_bus, 16)))

    return entries


def parse_table(paths, temp_path):
    """
    Parses a MCFG table
    """
    global _entries

    path = str()
    for p in paths:
        if os.path.isfile(p):
            path = p
            break
    if not path:
        message.addError("No ACPI MCFG table found, tried " + str(paths),
                         False)

    print("> Decompiling MCFG table " + path)
    data = acpi.decompile_table(path, temp_path,
                                "Could not obtain MCFG information; "
                                "PCI MMCONF information not found.")

    print("> Parsing MMCONF definitions")
    _entries = _parse_mmconfs(data)


def get_entries():
    """
    Return MMCONF information extracted from MCFG table
    """
    return _entries
