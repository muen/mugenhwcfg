#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module to select and store the schema data
import sys
import os
import paths
import importlib

# Global variable declaring binding module
schema = None


def selectSchema(modulename):
    "Choosing the module containing the PyXB bindings, call this first"
    sys.path.append(os.path.dirname(paths.SCHEMA_BINDING_PATH))
    imported = importlib.import_module(modulename)
    global schema
    schema = imported
    print("Binding module to be used: '%s'." % (schema.__file__))
