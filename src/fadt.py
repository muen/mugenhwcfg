#   Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2017 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module that handles extraction of data from the FADT ACPI table

import re

from . import acpi

_block_addr = re.compile(r'.*PM1A Control Block Address : '
                         '(?P<address>[0-9a-fA-F]+)')

_pm1a_cnt = ""


def _get_pm1a_cnt_port(data):
    """
    Return PM1A Control Block Address from FADT.
    """
    match = _block_addr.search(data)
    return "0x" + match.group("address").lower()


def parse_table(path, temp_path):
    """
    Parses a FADT table.
    """
    print("> Decompiling FADT table")
    data = acpi.decompile_table(path, temp_path,
                                "Could not obtain information from FADT; "
                                "system board shutdown port not found.")
    global _pm1a_cnt
    _pm1a_cnt = _get_pm1a_cnt_port(data)


def get_shutdown_port():
    """
    Return I/O port for system shutdown as declared in ACPI FADT table.
    """
    return _pm1a_cnt
