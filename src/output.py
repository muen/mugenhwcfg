#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module to handle generation of output file
import os
import paths
from . import message
import subprocess
from . import parseutil
from . import customExceptions
import importlib

SPACES_MAIN = 5
SPACES_SUB = 7


def produce_toolver():
    "Produces tool version using git describe"
    success = True
    result = produceLine("Generated with mugenhwcfg.", SPACES_MAIN)
    try:
        version = _runCommand("git describe --always",
                              "Tool version in output description could not "
                              "be obtained.")
    except customExceptions.FailedOutputCommand:
        success = False
    else:
        result = produceLine("Generated with mugenhwcfg (commit %s)" % version,
                             SPACES_MAIN)

    return result, success


def _runCommand(commandstr, errmsg):
    result = None
    command = commandstr.split()
    try:
        subprocess.check_call(command,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
    except (subprocess.CalledProcessError, OSError) as e:
        warning = (errmsg + " Subprocess '%s' encountered an error > %s" %
                   (commandstr, e))
        message.addWarning(warning)
        raise customExceptions.FailedOutputCommand(str(e))
    else:
        proc = subprocess.Popen(command,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        result = proc.communicate()[0].strip()

    return result.decode()


def produce_linuxver():
    "Produces linux version using uname"
    success = True
    result = produceLine("Linux kernel version: ", SPACES_MAIN)
    try:
        version = _runCommand("uname -r", "Linux kernel version in output "
                              "description could not be obtained.")
    except customExceptions.FailedOutputCommand:
        success = False
    else:
        result = produceLine("Linux kernel version: %s" % version, SPACES_MAIN)

    return result, success


def produce_distver():
    "Produces distribution version using lsb_release"
    success = True
    DESCRIPTION_KEY = "Description"
    result = produceLine("Distribution: ", SPACES_MAIN)
    try:
        distdata = _runCommand("lsb_release -d",
                               "Dist version in output description could not "
                               "be obtained.")
    except customExceptions.FailedOutputCommand:
        success = False
    else:
        try:
            distdesc = parseutil.parseData_Sep(distdata, DESCRIPTION_KEY, ":")
        except customExceptions.KeyNotFound:
            success = False
        else:
            result = produceLine("Distribution: %s" % distdesc, SPACES_MAIN)

    return result, success


def produce_boardinfo():
    "Produces board information"
    success = True
    result = produceLine("Board information:", SPACES_MAIN)
    try:
        dmiparser = parseutil.DMIParser(paths.DMI)
        result += produceLine("Vendor: %s" % dmiparser.getData("board_vendor"),
                              SPACES_SUB)
        result += produceLine("Name: %s" % dmiparser.getData("board_name"),
                              SPACES_SUB)
        result += produceLine("Version: %s" %
                              dmiparser.getData("board_version"),
                              SPACES_SUB)
    except Exception as e:
        message.addWarning("Board information in output description could not "
                           "be obtained: %s" % e)
        success = False
    return result, success


def produce_biosinfo():
    "Produces bios information"
    success = True
    result = produceLine("BIOS information:", SPACES_MAIN)
    try:
        dmiparser = parseutil.DMIParser(paths.DMI)
        result += produceLine("Vendor: %s" % dmiparser.getData("bios_vendor"),
                              SPACES_SUB)
        result += produceLine("Version: %s" %
                              dmiparser.getData("bios_version"),
                              SPACES_SUB)
        result += produceLine("Date: %s" % dmiparser.getData("bios_date"),
                              SPACES_SUB)
    except Exception as e:
        message.addWarning("BIOS information in output description could not "
                           "be obtained: %s" % e)
        success = False
    return result, success


def produce_log():
    "Append warnings and errors during generation"
    result = ""
    if message.messagequeue:
        result = produceLine("Log:", SPACES_MAIN)
        for msg in message.messagequeue:
            result += produceLine(msg.to_str(), SPACES_SUB)
    return result, True


def produce_productinfo():
    "Produces product information"
    success = True
    result = produceLine("Product information:", SPACES_MAIN)
    try:
        dmiparser = parseutil.DMIParser(paths.DMI)
        result += produceLine("Vendor: %s" %
                              dmiparser.getData("product_vendor"),
                              SPACES_SUB)
        result += produceLine("Name: %s" %
                              dmiparser.getData("product_name"),
                              SPACES_SUB)
        result += produceLine("Product Version: %s" %
                              dmiparser.getData("product_version"),
                              SPACES_SUB)
    except Exception as e:
        message.addWarning("Product information in output description could "
                           "not be obtained: %s" % e)
        success = False
    return result, success


def produceHeader(borderwidth, *producers):
    "Produces descriptive header comment code"
    topborder = "<!-- " + ("=" * borderwidth) + "\n"
    header = topborder
    for producer in producers:
        header += producer()[0]
    header += "     " + ("=" * borderwidth) + " -->\n"

    return header


def produceLine(info, lspace):
    "Produces a line in the header"
    result = ""
    if info != "":
        for line in info.splitlines():
            result += " " * lspace
            result += line
            result += "\n"

    return result


def formatXML(xmlstr, encoding):
    "Uses lxml to format xml string"
    print("Formatting XML document...")
    result = xmlstr
    success = True
    try:
        formatmodule = importlib.import_module("lxml.etree")
    except ImportError:
        message.addWarning(
            "LXML library not found, could not format XML document.")
        result = b''
        success = False
    else:
        root = formatmodule.fromstring(xmlstr)
        # Required to enforce alphabetical attribute ordering
        # lxml supports this via C14N serialization, see
        # https://lxml.de/api.html#c14n
        root = formatmodule.fromstring(formatmodule.canonicalize(root))
        result = formatmodule.tostring(root, pretty_print=True,
                                       xml_declaration=True,
                                       encoding=encoding)

    return result.decode(), success


def genXML(elemtree, encoding):
    "Produce entire xml string from elemtree"
    BORDER_WIDTH = 42
    xmlstr = elemtree.toxml(encoding, element_name="hardware")
    xml = formatXML(xmlstr, encoding)[0]

    # Find declaration
    xmltokens = xml.partition("?>")
    xml_declare = xmltokens[0] + xmltokens[1]
    xml_body = xmltokens[2]

    # Produce header
    header = produceHeader(
        BORDER_WIDTH,
        produce_toolver,
        produce_linuxver,
        produce_distver,
        produce_productinfo,
        produce_boardinfo,
        produce_biosinfo,
        produce_log,
    )

    # Combine declaration with header and body
    xml = xml_declare + '\n' + header + '\n' + xml_body

    return xml


def output(xml, outpath):
    OUTPUT_NAME = os.path.basename(outpath)

    print("> XML file '%s' generated to location: \n %s" % (
        OUTPUT_NAME, outpath))

    with open(outpath, "w") as f:
        for line in xml.splitlines(True):
            f.write(line)
