#   Copyright (C) 2016 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2016 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# ACPI utility functions

import os
import shutil
import subprocess
import uuid

from . import extractor
from . import message


def _run_iasl(prefix):
    """
    Run iasl tool with given path prefix
    """
    return subprocess.check_output(["iasl", "-p", prefix, "-d",
                                   prefix + ".dat"])


def _run_acpiexec(command, path, errmsg):
    """
    Run acpiexec tool with given command on file specified by path.
    """
    output = ""
    try:
        output = subprocess.check_output(["acpiexec", "-di", "-b", command,
                                         path])
    except OSError:
        message.addMessage("Error running acpiexec tool. Maybe it is not " +
                           "installed? Try 'apt-get install acpica-tools'" +
                           " to install.")
        message.addError(errmsg, False)

    return output


def _copy_acpi_table(source, dstdir, suffix, errmsg):
    """
    Copy ACPI table specified by source to destination directory. On success,
    the name of the destination file without suffix is returned, empty string
    otherwise.
    """
    if not os.path.isfile(source):
        message.addMessage("No ACPI file found at: '%s'; " % source)
        return ""

    if not os.path.isdir(dstdir):
        message.addMessage("Destination directory does not exist: '%s'; "
                           % dstdir)
        return ""

    dstfile = dstdir + "/" + str(uuid.uuid4())

    # Copy table to temp folder
    try:
        shutil.copyfile(source, dstfile + suffix)
    except IOError:
        message.addMessage("ACPI table at: '%s' " % source + "could not "
                           "be copied to location: '%s'" % dstfile + suffix)
        message.addError(errmsg, False)
        return ""

    return dstfile


def decompile_table(tblfile, tmpdir, errmsg):
    """
    Decompile ACPI table file with specified path using the iasl utility
    """
    suffix = ".dat"
    content = ""

    dstfile = _copy_acpi_table(tblfile, tmpdir, suffix, errmsg)
    if dstfile == "":
        return ""

    dsl_file = dstfile + ".dsl"

    try:
        _run_iasl(dstfile)
    except OSError:
        message.addMessage("Error running iasl tool. Maybe it is not " +
                           "installed? Try 'apt-get install acpica-tools'" +
                           " to install.")
        message.addError(errmsg, False)
    else:
        try:
            content = extractor.extractData(dsl_file)
        except IOError:
            message.addError("Error extracting data from decompiled ACPI table"
                             " %s." % dsl_file, False)
    try:
        os.remove(dsl_file)
    except OSError:
        pass

    return content


def execute_aml(amlfile, command, tmpdir, errmsg):
    """
    Execute specified command(s) on given AML file using the acpiexec utility
    and return the output as string. An empty string is returned on failure.
    """
    dstfile = _copy_acpi_table(amlfile, tmpdir, "", errmsg)
    if dstfile == "":
        return ""

    return _run_acpiexec(command, dstfile, errmsg).decode()
