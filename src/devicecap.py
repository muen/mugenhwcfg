#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module to handle device capabilities
# From setpci --dumpreg command and Linux/include/uapi/linux/pci_regs.h
# This list can be appended when more capability IDs are found
from . import extractor
import os
from . import customExceptions
from . import util
import math
import struct
from collections import namedtuple

# CAPABILITY CODES
CAP_PM = "0x01"
CAP_AGP = "0x02"
CAP_VPD = "0x03"
CAP_SLOTID = "0x04"
CAP_MSI = "0x05"
CAP_CHSWP = "0x06"
CAP_PCIX = "0x07"
CAP_HT = "0x08"
CAP_VNDR = "0x09"
CAP_DBG = "0x0a"
CAP_CCRC = "0x0b"
CAP_HOTPLUG = "0x0c"
CAP_SSVID = "0x0d"
CAP_AGP3 = "0x0e"
CAP_SECURE = "0x0f"
CAP_EXP = "0x10"
CAP_MSIX = "0x11"
CAP_SATA = "0x12"
CAP_AF = "0x13"

translated = {
    CAP_PM: "Power Management",
    CAP_AGP: "Accelerated Graphics Port",
    CAP_VPD: "Vital Product Data",
    CAP_SLOTID: "Slot Identification",
    CAP_MSI: "Message Signaled Interrupts",
    CAP_CHSWP: "CompactPCI Hotswap",
    CAP_PCIX: "PCI-X",
    CAP_HT: "HyperTransport",
    CAP_VNDR: "Vendor-Specific",
    CAP_DBG: "Debug Port",
    CAP_CCRC: "CompactPCI Central Resource Control",
    CAP_HOTPLUG: "PCI Standard Hot-Plug Controller",
    CAP_SSVID: "Bridge subsystem vendor/device ID",
    CAP_AGP3: "AGP Target PCI-PCI bridge",
    CAP_SECURE: "Secure Device",
    CAP_EXP: "PCI Express",
    CAP_MSIX: "MSI-X",
    CAP_SATA: "SATA Data/Index Configuration",
    CAP_AF: "PCI Advanced Features"
}


Cap = namedtuple("Cap", ["code", "value"])

# CAPABILITY VALUES (CapValue objects)
CAP_MSI_VALUE = namedtuple("MSI", "enable, count")
CAP_MSIX_VALUE = namedtuple("MSIX", "enable, count")

# CAPABILITY VALUE ACTIONS (on data after nextpointer)

# PCI Config space offset for interrupt info, see PCI Local Bus Specification,
# section 6.1.
INTERRUPT_LINE = 0x3c
INTERRUPT_PIN = 0x3d

Interrupt_Info = namedtuple("int_info", "line, pin")


def get_cap_msi(fileobj):
    """
    Parse PCI config space MSI capability and return if MSI is enabled and how
    many MSI interrupt vectors are supported by the device, see PCI Local Bus
    Specification, Revision 3.0, section 6.8.1.3.
    """
    byte = struct.unpack('B', fileobj.read(1))[0]
    enablebit = util.getBit(byte, 0)     # bit  0
    multimsgcapable = (byte & 0xe) >> 1  # bits [3..1]
    count = int(math.pow(2, multimsgcapable))
    value = CAP_MSI_VALUE(enable=enablebit, count=count)
    return value


def get_cap_msix(fileobj):
    """
    Parse PCI config space MSI-x capability and return if MSI is enabled and
    how many MSI interrupt vectors are supported by the device, see PCI Local
    Bus Specification, Revision 3.0, section 6.8.2.3.
    """
    msgcontrol = struct.unpack('H', fileobj.read(2))[0]
    enablebit = util.getBit(msgcontrol, 15)  # bit  15
    tablesize = (msgcontrol & 0x7ff)         # bits [10..0]
    count = tablesize + 1
    value = CAP_MSIX_VALUE(enable=enablebit, count=count)
    return value


def readdata(f, size):
    """
    Read size number of bytes from file f and return as integer.
    """
    data = f.read(size)
    if data != b'':
        # returns integer of data
        return struct.unpack('B', data)[0]
    else:
        raise customExceptions.NoAccessToFile(
            "No permission to read file: %s" % f)


capswitcher = {
    CAP_MSI: get_cap_msi,
    CAP_MSIX: get_cap_msix
}


class DevicecapManager():

    "Class that handles PCI device capability data"

    def __init__(self):
        self.capabilities = {}  # devicepath, caplist
        self.int_info = {}      # devicepath, interrupt info

    def extractCapabilities(self, devicepaths):

        """
        Checks if device capabilities can be found and creates capability
        dict"""

        # Call this function first to attempt to fill dictionary
        for devicepath in devicepaths:
            try:
                self._extractCapability(devicepath, "config")
            except (customExceptions.NoAccessToFile, IOError):
                raise customExceptions.DeviceCapabilitiesNotRead(
                    "Could not read capability: %s" % devicepath)

    def getCapList(self, devicepath, simple=True):
        "Returns list of capabilities, only codes if simple=True"
        result = []
        caplist = self.capabilities.get(devicepath)
        if caplist is not None:
            result = []
            if simple:
                for cap in caplist:
                    result.append(cap.code)
            else:
                result = caplist
        return result

    def getCapValue(self, devicepath, capcode):

        """
        Returns CapValue tuple for a device (additional information on
        capability)"""

        result = None
        caplist = self.capabilities.get(devicepath)
        if caplist is not None:
            for cap in caplist:
                if cap.code == capcode:
                    return cap.value
        return result

    def getCapabilities(self):
        return self.capabilities

    def getInterruptInfo(self, devicepath):
        """
        Returns interrupt info for a device specified by path. If no info for
        the given path is present an interrupt info with a line of 255 and pin
        set to 0 is returned.
        """
        result = self.int_info.get(devicepath)
        if result is None:
            result = Interrupt_Info(line=255, pin=0)

        return result

    def _extractCapability(self, devicepath, configfilename):
        "Gets capability for device"
        CAPABILITY_START = 0x34
        STATUS_REG_LOCATION = 0x6
        CAPABILITY_BIT_POS = 4
        NEXT_OFFSET = 1
        CAP_SIZE = 1
        STOP_ID = 0x00
        CAPABILITY_NUM = 48
        CONFIG_PATH = os.path.join(devicepath, configfilename)

        print("Reading device data : ", CONFIG_PATH)

        devfile = open(CONFIG_PATH, "rb")
        try:
            # Checks config file whether capability bit is activated
            capbyte = extractor.extractBinaryData(
                CONFIG_PATH, STATUS_REG_LOCATION, 1)
            capint = int(capbyte, 16)
            if util.getBit(capint, CAPABILITY_BIT_POS):
                # Checks config file, starting at CAPABILITY_START and moving
                # through linked list
                self.capabilities[devicepath] = self._readCapFile(
                    devfile, CAPABILITY_START, CAP_SIZE, NEXT_OFFSET,
                    STOP_ID, CAPABILITY_NUM)

            self.int_info[devicepath] = self._readInterruptInfo(devfile)
        finally:
            if devfile is not None:
                devfile.close()

    def _readCapFile(self, devfile,
                     startpos,
                     capsize,
                     nextoffset=1,
                     stopid=0x00,
                     numJumps=-1):
        """
        Extracts data from the config file that is in linked list format i.e
        reads address from startpos, reads data in address, reads address from
        ptroffset..."""
        result = []

        devfile.seek(startpos)
        nextaddr = readdata(devfile, capsize)
        while (nextaddr != stopid and numJumps != 0):
            devfile.seek(nextaddr)
            data = readdata(devfile, capsize)               # read capcode
            capcode = "0x{:02x}".format(data)
            nextaddr = readdata(devfile, capsize)           # read next address
            capvalue = self._getCapValue(capcode, devfile)  # read cap info
            cap = Cap(code=capcode, value=capvalue)
            result.append(cap)
            numJumps -= 1

        return result

    def _readInterruptInfo(self, devfile):
        """
        Extract interrupt line and pin information from PCI config space passed
        in as open file descriptor.
        """
        devfile.seek(INTERRUPT_LINE)
        int_line = readdata(devfile, 1)
        devfile.seek(INTERRUPT_PIN)
        int_pin = readdata(devfile, 1)

        return Interrupt_Info(line=int_line, pin=int_pin)

    def _getCapValue(self, capcode, fileobj):
        """
        Gets the extra information for capabilities in form of tuple.
        Returns None if no Value found.
        """
        def result(x):
            return None
        try:
            result = capswitcher[capcode]
        except KeyError:
            pass
        return result(fileobj)  # Perform function based on capcode


def translate(capcode):
    try:
        return translated[capcode]
    except KeyError:
        raise customExceptions.CapabilityUnknown(
            "Capability Code %s is unknown." % capcode)
