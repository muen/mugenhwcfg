#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


import os
import re
import subprocess
from collections import namedtuple, OrderedDict

import paths
from . import dmar
from . import dsdt
from . import fadt
from . import madt
from . import mcfg
from . import parseutil
from . import customExceptions
from . import util
from . import output
from . import message
from . import devicecap
from . import extractor
from . import schemadata
from . import msrs

Address = namedtuple("Address", "start end")
SerialDevice = namedtuple("SerialDevice", "name start end irq")
PciDevice = namedtuple(
    "PciDevice", "name classcode vendorId deviceId revisionId descr")

PAGE_SIZE = "0x1000"  # 4KB
PAGE_MIN_SIZE = PAGE_SIZE
MEM_ALLOCATABLE_MINSIZE = 0x100000  # 1 MB
PCI_BUS_MMCONF_SIZE = 0x100000  # 1 MB
PROCESSOR_SPEED_KEYWORDS = ["GHz", "MHz"]
RESOURCE_START_IDX = 1
WELL_KNOWN_IOPORTS = {
    Address("3f8", "3ff"): "com_1",
    Address("2f8", "2ff"): "com_2",
    Address("3e8", "3ef"): "com_3",
    Address("2e8", "2ef"): "com_4"
}
DEFAULT_IOAPIC_SID = "16#f0f8#"
DEFAULT_IRQ_NUMBER = "16"
MAX_IRQ_NUMBER = 220


def getPciConfigAddress():
    pciconfigaddr = ""
    try:
        pciconfigaddr = hex(mcfg.get_entries()[0].address)
        if len(pciconfigaddr) == 0:
            raise Exception
    except Exception:
        message.addWarning(
            "Could not obtain pciConfigAddress from MCFG.")

    return pciconfigaddr


def getPciConfigSize():
    pciconfigsize = ""
    try:
        entries = mcfg.get_entries()
        bus_start = entries[0].start
        bus_end = entries[0].end
        if bus_start > bus_end:
            raise Exception

        pciconfigsize = hex((bus_end - bus_start + 1)
                            * PCI_BUS_MMCONF_SIZE)

    except Exception:
        message.addWarning("Could not obtain pciConfigSize from MCFG.")

    return pciconfigsize


class ProcessorCreator():

    def createElem(self, cpuinfopath, msrpaths, dmesgpath):
        print("> Creating element: processor")

        VMX_OFFSET = 0x485
        VMX_BITSIZE = 5
        rate = self.getVmxTimerRate(msrpaths, VMX_OFFSET, VMX_BITSIZE)
        cores = self.get_cpu_cores(cpuinfopath)

        processor = schemadata.schema.processorType(
            cpuCores=cores,
            speed=round(self.getSpeed(dmesgpath)),
            vmxTimerRate=rate)

        apic_ids = self.get_apic_ids(cpuinfopath)
        if len(apic_ids) != int(cores):
            message.addError("Mismatching CPU core or APIC ID count")
        for apic in apic_ids:
            processor.append(schemadata.schema.cpuCoreType(apicId=apic))

        cpuid_values = self.get_cpuid_data()
        if len(cpuid_values) == 0:
            message.addWarning("Unable to extract CPUID information")
        else:
            for cpuid in cpuid_values:
                processor.append(schemadata.schema.cpuIDValueType(
                    leaf=cpuid['leaf'], subleaf=cpuid['subleaf'],
                    eax=cpuid['eax'], ebx=cpuid['ebx'],
                    ecx=cpuid['ecx'], edx=cpuid['edx']))

        msr_values = msrs.get_msrs_of_interest(msrpaths)
        if len(msr_values) == 0:
            message.addWarning("Unable to extract MSR values")
        else:
            for msr in msr_values:
                processor.append(schemadata.schema.msrValueType(
                    address=msr['address'], name=msr['name'],
                    regval=msr['regval']))

        print("Element created: processor")
        return processor

    def get_apic_ids(self, cpuinfopath):
        pattern = re.compile(r'^initial apicid\s:\s([0-9]+)', re.MULTILINE)
        ids = []
        for entry in pattern.finditer(extractor.extractData(cpuinfopath)):
            id = int(entry.group(1))
            if id % 2 == 0:
                ids.append(id)
        return ids

    def get_cpuid_data(self):
        raw = output._runCommand("taskset 0x1 cpuid -1 -r",
                                 "Unable to execute cpuid")
        pattern = re.compile(r'(0x[0-9a-fA-F]{8}) (0x[0-9a-fA-F]{2}): '
                             'eax=(0x[0-9a-fA-F]{8}) ebx=(0x[0-9a-fA-F]{8}) '
                             'ecx=(0x[0-9a-fA-F]{8}) edx=(0x[0-9a-fA-F]{8})')
        values = []
        for entry in pattern.finditer(raw):
            value = {'leaf': util.toWord64(entry.group(1), retainzeros=True),
                     'subleaf': '16#' + entry.group(2)[2:] + '#',
                     'eax': util.toWord64(entry.group(3), retainzeros=True),
                     'ebx': util.toWord64(entry.group(4), retainzeros=True),
                     'ecx': util.toWord64(entry.group(5), retainzeros=True),
                     'edx': util.toWord64(entry.group(6), retainzeros=True)}
            values.append(value)
        return values

    def get_cpu_cores(self, cpuinfopath):
        core_entry = re.compile(r'^physical id\s:\s([0-9]+)(?:.*\n){3}'
                                r'cpu cores\s:\s([0-9]+)', re.MULTILINE)
        cpu_info = {}

        for entry in core_entry.finditer(extractor.extractData(cpuinfopath)):
            phys_id = int(entry.group(1))
            cores = int(entry.group(2))

            if phys_id not in cpu_info:
                cpu_info[phys_id] = cores

        return sum(cpu_info.values())

    def getSpeed(self, dmesgpath):
        "Gets speed value from dmesg"
        regs = [re.compile(r'.*Refined TSC clocksource calibration: '
                           '(?P<speed>.*)'),
                re.compile(r'.*tsc: Detected (?P<speed>.*) processor')]

        data = ""
        result = 0
        try:
            data = extractor.extractData(dmesgpath)
        except IOError:
            errstr = ("Could not read file: %s\n" % dmesgpath +
                      "Processor speed not found.")
            message.addError(errstr)

        for regex in regs:
            match = regex.search(data)
            if match is not None:
                result = util.getSpeedValue(match.group("speed"),
                                            PROCESSOR_SPEED_KEYWORDS)
                break

        if not result:
            message.addError("Could not find TSC speed in: %s\n" % dmesgpath)

        return result

    def getVmxTimerRate(self, msrpaths, offset, vmxbitsize):
        # check for MSR
        vmx_timer_rate = msrs.read_msr(msrpaths, offset, 1)

        vmxbits = 0
        # Get bits from VMX_BITS_START to VMX_BITS_END
        for bitnum in range(0, vmxbitsize):
            vmxbits += util.getBit(int(vmx_timer_rate, 16), bitnum) << bitnum
        vmx_timer_rate = int(vmxbits)

        return vmx_timer_rate


class MemoryCreator():

    def createElem(self, memmappath):
        print("> Creating element: memory")

        memory = schemadata.schema.physicalMemoryType()
        # Get list of memoryBlocks available
        memoryBlockList = self.getMemoryBlocks(memmappath)
        for memoryBlock in memoryBlockList:
            memory.append(memoryBlock)

        for region in dmar.get_reserved_mem_regions():
            memory.append(self.generate_reserved_mem_region(region))

        print("Element created: memory")
        return memory

    def getMemoryBlocks(self, path):
        memoryBlockList = []

        def walkError(excep):
            message.addError("Could not access memory block data: " +
                             str(excep), False)

        memdirs = []
        for root, subdirs, files in os.walk(path, onerror=walkError):
            if not subdirs:  # at end of paths
                memdirs.append(root)

        memdirs.sort(key=lambda x: int(os.path.basename(x)))
        # Sort paths found by numerical order
        for root in memdirs:
            endfile = root + "/" + "end"
            typefile = root + "/" + "type"
            startfile = root + "/" + "start"
            try:
                memoryBlock = self.generateMemoryBlock(endfile,
                                                       typefile,
                                                       startfile)
            except IOError:
                message.addError("Could not retrieve complete memory data",
                                 False)
            else:
                # Adds newly created memoryBlock element to memoryBlockList
                memoryBlockList.append(memoryBlock)

        # Filter out memoryBlocks that do not meet requirements
        memoryBlockList = self.filterMemoryBlocks(memoryBlockList)

        return memoryBlockList

    def filterMemoryBlocks(self, memoryBlockList):
        "Removes reserved and empty memory blocks and returns result"
        result = []
        print("Filtering memory blocks...")
        filterlist = [mem for mem in memoryBlockList
                      if mem.name.lower() == "reserved" or
                      mem.size == "16#0000#"]
        result = util.removeListsFromList(memoryBlockList, filterlist)
        return result

    def generateMemoryBlock(self, endfile, typefile, startfile):
        name = extractor.extractData(typefile)
        physical_addr = extractor.extractData(startfile)

        memsize = util.sizeOf(extractor.extractData(endfile),
                              extractor.extractData(startfile))
        # Round memsize down to multiple of PAGE_SIZE.
        # Note: Blocks with size below one page will be rounded down to
        # zero.
        if int(memsize, 16) % int(PAGE_SIZE, 16) != 0:
            memrounded = util.hexRoundToMultiple(
                memsize, PAGE_SIZE, rounddown=True)
            print("Mem size %s for memoryBlock %s rounded down to: %s" % (
                memsize, physical_addr, memrounded))
            memsize = memrounded

        allocatable = str(self.isAllocatable(name, physical_addr)).lower()

        memoryBlock = schemadata.schema.memoryBlockType(
            name=name, physicalAddress=util.toWord64(physical_addr),
            size=util.toWord64(memsize),
            allocatable=allocatable)

        return memoryBlock

    def isAllocatable(self, name, physical_addr):
        if (name == "System RAM" and
           int(physical_addr, 16) >= MEM_ALLOCATABLE_MINSIZE):
            return True
        else:
            return False

    def generate_reserved_mem_region(self, region):
        return schemadata.schema.reservedMemRegionType(
            name=region.name, physicalAddress=util.toWord64(region.start),
            size=util.toWord64(util.sizeOf(region.end, region.start)))


class DevicesCreator():

    def createElem(self, dmesg):
        print("> Creating element: devices")
        devices = schemadata.schema.devicesType(
            pciConfigAddress=util.toWord64(getPciConfigAddress()),
            pciConfigSize=util.toWord64(getPciConfigSize()))

        # Add system board
        print("> Extracting system board information...")
        devices.append(SystemboardDeviceCreator().create_element())

        # Add I/O APICs
        print("> Extracting I/O APIC information...")
        for dev in IoapicDevicesCreator().createElems(dmesg):
            devices.append(dev)

        # Add IOMMUs
        print("> Extracting IOMMU device information...")
        for dev in IommuDevicesCreator().createElems(paths.DEVMEM):
            devices.append(dev)

        # Add Serial Devices
        print("> Extracting Serial device information...")
        for dev in SerialDevicesCreator().createElems(paths.SERIALS):
            devices.append(dev)

        # Add Pci Devices
        print("> Extracting PCI device information...")
        for dev in PciDevicesCreator().createElems(paths.DEVICES,
                                                   paths.IOMMUGRPS):
            devices.append(dev)

        print("Element created: devices")

        return devices


class PciDevicesCreator():

    "Helper class of DevicesCreator"

    def __init__(self):
        self.devicenames = {}  # devicepath = name

    def createElems(self, devicesdir, iommugrpdir):
        pcidevicelist = []
        devicepaths = []
        devicecapmgr = None
        devicespecs = {}
        print("Finding PCI devices...")
        # Get device absolute paths from symbolic links in paths.DEVICES
        devicepaths = util.getLinks(devicesdir, self.isDeviceName)
        print("Checking Dependencies...")
        devicecapmgr = self.init_DevicecapManager(devicepaths)
        devicespecs = self.getDeviceSpecs(devicepaths, paths.PCIIDS)
        print("Examining PCI devices...")
        filteredpaths = self.filterDevicePaths(devicepaths, devicecapmgr)
        print(("Extracting device information from %d PCI devices " %
              len(filteredpaths) + "(excluding PCI bridges and non "
              "PCI-Express devices behind bridges)..."))
        if (not os.path.isdir(iommugrpdir) or not os.listdir(iommugrpdir)):
            message.addWarning("IOMMU groups not available in directory '" +
                               iommugrpdir + "', reboot with "
                               "'intel_iommu=on' kernel parameter.")
            iommugrpdir = None

        for devicepath in filteredpaths:
            pcidevicelist.append(self.createDeviceFromPath(
                devicepath,
                devicecapmgr,
                devicespecs[devicepath],
                iommugrpdir))

        return pcidevicelist

    def init_DevicecapManager(self, devicepaths):
        "Initialises the DeviceCapManager"

        print("Getting device capabilities...")
        devicecapmgr = devicecap.DevicecapManager()
        try:
            devicecapmgr.extractCapabilities(devicepaths)
        except customExceptions.DeviceCapabilitiesNotRead:
            message.addError("Not enough permissions to access capabilities "
                             "of devices. It is advised to run the tool again "
                             "with the proper permissions.", False)
        return devicecapmgr

    def filterDevicePaths(self, devicePaths, devicecapmgr):
        "Returns filtered list of paths of devices"
        bridgePaths = []
        pciExpressPaths = []
        bridgedDevicePaths = []
        nonPciExpressPaths = []
        resultPaths = []
        for devicepath in devicePaths:
            if self.isPciExpress(devicepath, devicecapmgr):
                pciExpressPaths.append(devicepath)

            if self.isBridge(devicepath):
                bridgePaths.append(devicepath)
                for root, subdirs, files in os.walk(devicepath):
                    for subdir in subdirs:
                        if self.isDeviceName(subdir):
                            bridgedDevicePaths.append(
                                os.path.join(root, subdir))

        for bridgedDevice in bridgedDevicePaths:
            if self.isPciExpress(bridgedDevice, devicecapmgr) is False:
                nonPciExpressPaths.append(bridgedDevice)

        print("PCI Devices found: %d\n------------------" % len(devicePaths))
        print("> PCI Bridges:", len(bridgePaths))
        for item in bridgePaths:
            print("  ", os.path.basename(item))

        print("> Devices behind bridges:", len(bridgedDevicePaths))
        for item in bridgedDevicePaths:
            print("  ", os.path.basename(item))

        print("> PCI Express Devices:", len(pciExpressPaths))
        for item in pciExpressPaths:
            print("  ", os.path.basename(item))

        resultPaths = util.removeListsFromList(devicePaths,
                                               bridgePaths,
                                               nonPciExpressPaths)
        return resultPaths

    def isBridge(self, devicepath):
        PCI_BRIDGE = "0x0604"
        dev_class = extractor.extractData(os.path.join(devicepath,
                                                       "class"))[0:6]
        return dev_class == PCI_BRIDGE

    def isPciExpress(self, devicepath, devicecapmgr):
        PCI_EXPRESS = "0x10"
        return PCI_EXPRESS in devicecapmgr.getCapList(devicepath)

    def isDeviceName(self, value):
        "Checks for format: ####:##:##.#"
        splitcolon = value.split(':')
        result = True

        try:
            if len(splitcolon) != 3:
                result = False

            if '.' not in splitcolon[2]:
                result = False

            if len(splitcolon[0]) != 4:  # Host bus no. length
                result = False

            if len(splitcolon[1]) != 2:  # Bus no. length
                result = False

            if len(splitcolon[2].split('.')[0]) != 2:  # Device no. length
                result = False

            if len(splitcolon[2].split('.')[1]) != 1:  # Function no. length
                result = False
        except IndexError:
            result = False

        return result

    def getDeviceBus(self, devicestr):
        return devicestr.split(':')[1]

    def getDeviceNo(self, devicestr):
        return (devicestr.split(':')[2]).split('.')[0]

    def getDeviceFunction(self, devicestr):
        return (devicestr.split(':')[2]).split('.')[1]

    def getDeviceSpecs(self, devicepaths, pciids):
        specs = OrderedDict()
        occurrences = {}

        try:
            # Init PciIdsParser, throws customExceptions.PciIdsFileNotFound
            # if fail
            pciIdsParser = parseutil.PciIdsParser(pciids)
        except customExceptions.PciIdsFileNotFound:
            message.addError("pci.ids file could not be located in tool "
                             "directory: %s. " % paths.CURRENTDIR +
                             "Device "
                             "names could not be obtained. Please ensure that "
                             "the file is in the directory.",
                             False)
        else:

            for devicepath in devicepaths:
                classcode = extractor.extractData(
                    os.path.join(devicepath, "class"))[2:6]
                vendor = extractor.extractData(
                    os.path.join(devicepath, "vendor"))[2:6]
                device = extractor.extractData(
                    os.path.join(devicepath, "device"))[2:6]
                revision = extractor.extractBinaryData(
                    os.path.join(devicepath, "config"), 8, 1)[2:4]

                # Construct device name using class name
                # (e.g. usb_controller_1).
                device_name = self.toClassName(classcode, pciIdsParser)
                cur_count = occurrences.get(device_name, 1)
                occurrences[device_name] = cur_count + 1

                descr = ""
                try:
                    descr = pciIdsParser.getVendorName(
                        vendor) + " " + pciIdsParser.getDeviceName(vendor,
                                                                   device)
                except Exception:
                    descr = ""

                specs[devicepath] = PciDevice(
                    name=device_name + "_%d" % cur_count,
                    classcode=util.wrap16(classcode),
                    vendorId=util.wrap16(vendor),
                    deviceId=util.wrap16(device),
                    revisionId=util.wrap16(revision),
                    descr=descr)

        return specs

    def toClassName(self, classcode, pciIdsParser):
        "Convert given class code to name"
        classname = classcode
        try:
            classname = pciIdsParser.getClassName(classname)
            classname = util.spacesToUnderscores(classname.lower())
        except (customExceptions.PciIdsFailedSearch,
                customExceptions.PciIdsSubclassNotFound):
            message.addWarning(("Unable to resolve device class " + classcode +
                                ". Please update pci.ids (-u) and try again"))
        return classname

    def getPci(self, devicepath, devicecapmgr, devicespec, iommugrpdir):
        pcistr = os.path.basename(devicepath)
        pci = schemadata.schema.pciType(
            bus=util.wrap16(self.getDeviceBus(pcistr)),
            device=util.wrap16(self.getDeviceNo(pcistr)),
            function=self.getDeviceFunction(pcistr)).append(
                schemadata.schema.deviceIdentificationType(
                    classcode=devicespec.classcode,
                    vendorId=devicespec.vendorId,
                    deviceId=devicespec.deviceId,
                    revisionId=devicespec.revisionId))

        if iommugrpdir:
            for root, dirs, files in os.walk(iommugrpdir):
                if pcistr in dirs:
                    tokens = root.split("/")
                    pci.append(schemadata.schema.iommuGroupType
                               (id=tokens[len(tokens) - 2]))

        return pci

    def getLegacyIrq(self, devicepath, devicecapmgr):
        PCI_BUS_PREFIX = "pci0000:"
        PREFIX_LEN = len(PCI_BUS_PREFIX)
        depth = 32
        irqs = []
        path = devicepath
        irqNr = 0

        while depth > 0:
            """
            Traverse PCI topology towards root complex until device connected
            to system bus is found.
            """
            depth = depth - 1
            pcistr = os.path.basename(path)
            parent_path = os.path.dirname(path)

            if int(self.getDeviceBus(pcistr), 16) == 0 or os.path.basename(
                    parent_path)[0:PREFIX_LEN] == PCI_BUS_PREFIX:
                break
            path = parent_path

        if depth == 0:
            errstr = ("Unable to determine PCI bridge device connected to "
                      "system bus for device '" + devicepath + "'.")
            message.addError(errstr)

        int_info = devicecapmgr.getInterruptInfo(path)
        if int_info.pin != 0 and int_info.line != 255:
            devno = int(self.getDeviceNo(pcistr), 16)
            acpi_pin_nr = int_info.pin - 1
            irqNr = dsdt.get_global_system_interrupt(devno, acpi_pin_nr)

        if irqNr is not None and irqNr > MAX_IRQ_NUMBER:
            message.addWarning("Invalid irq number " + str(irqNr)
                               + " for device '" + devicepath + "'.")

        if irqNr is not None and (irqNr > 0 and irqNr <= MAX_IRQ_NUMBER):
            try:
                irq = schemadata.schema.irqType(
                    name="irq%d" % RESOURCE_START_IDX, number=str(irqNr))
                irqs.append(irq)
            except Exception:
                message.addWarning("Skipping invalid IRQ resource for " +
                                   "device " + os.path.basename(devicepath)
                                   + ": " + str(irqNr))

        return irqs

    def getDeviceMemory(self, devicepath, minsize=PAGE_MIN_SIZE):
        devmemblocks = []
        try:
            resourceData = extractor.extractData(
                os.path.join(devicepath, "resource"))
        except IOError:
            message.addError("Could not obtain memory information for device: "
                             "%s" % os.path.basename(devicepath), False)
        else:
            memcount = RESOURCE_START_IDX
            for line in resourceData.splitlines():
                tokens = line.split(' ')
                if tokens[2][-3] == '2':  # if line represents a memory block

                    memory = schemadata.schema.deviceMemoryType(
                        name="mem%d" % memcount,
                        physicalAddress=util.toWord64(tokens[0]),
                        caching="UC")
                    # Rounds memsize up to minsize
                    memsize = util.sizeOf(tokens[1], tokens[0])
                    if int(memsize, 16) < int(minsize, 16):
                        memrounded = util.hexFloor(memsize, minsize)
                        print(("Mem size %s for device %s rounded to: %s" %
                              (memsize,
                               os.path.basename(devicepath),
                               memrounded)))
                        memsize = memrounded

                    memory.size = util.toWord64(memsize)
                    devmemblocks.append(memory)
                    memcount += 1

        return devmemblocks

    def getIoports(self, devicepath):
        ioports = []

        try:
            resourceData = extractor.extractData(
                os.path.join(devicepath, "resource"))
        except IOError:
            message.addError("Could not obtain ioport information for device: "
                             "%s" % os.path.basename(devicepath), False)
        else:
            ioportcount = RESOURCE_START_IDX
            for line in resourceData.splitlines():
                tokens = line.split(' ')
                if tokens[2][-3] == '1':  # if line represents ioport info

                    ioPort = schemadata.schema.ioPortType(
                        name="ioport%d" % ioportcount,
                        start=util.toWord64(tokens[0]),
                        end=util.toWord64(tokens[1]))
                    ioportcount += 1
                    ioports.append(ioPort)

        return ioports

    def getIrqs(self, devicepath, devicecapmgr):
        irqs = self.getLegacyIrq(devicepath, devicecapmgr)

        count = 0
        msicap = None
        cap_list = devicecapmgr.getCapList(devicepath)

        if devicecap.CAP_MSI in cap_list:
            msicap = devicecapmgr.getCapValue(devicepath, devicecap.CAP_MSI)
        if devicecap.CAP_MSIX in cap_list:
            msicap = devicecapmgr.getCapValue(devicepath, devicecap.CAP_MSIX)

        if msicap is not None:
            count = msicap.count

        if len(irqs) == 0 and count > 0:
            irq = schemadata.schema.irqType(name="irq1",
                                            number=DEFAULT_IRQ_NUMBER)
            irqs.append(irq)
            message.addWarning("Adding fake irq " + DEFAULT_IRQ_NUMBER
                               + " for device with MSIs: %s"
                               % os.path.basename(devicepath))

        for i in range(count):
            irqs[0].append(schemadata.schema.msiIrqType(name="msi"
                                                        + str(i + 1)))

        return irqs

    def get_mmconf(self, base, bus, devno, fn):
        "Return mmconf region for given device"
        addr = base + (bus * 2 ** 20 + devno * 2 ** 15 + fn * 2 ** 12)
        return schemadata.schema.deviceMemoryType(
            name="mmconf",
            physicalAddress=util.toWord64(str(hex(addr))),
            size="16#1000#",
            caching="UC")

    def createDeviceFromPath(self, devicepath, devicecapmgr, devicespec,
                             iommugrp):
        device = schemadata.schema.deviceType(name=devicespec.name)
        pcistr = os.path.basename(devicepath)

        bus = int(self.getDeviceBus(pcistr), 16)
        devno = int(self.getDeviceNo(pcistr), 16)
        fn = int(self.getDeviceFunction(pcistr), 16)

        if devicespec.descr:
            device.description = devicespec.descr

        device.pci = self.getPci(devicepath, devicecapmgr, devicespec,
                                 iommugrp)

        # irq
        for irq in self.getIrqs(devicepath, devicecapmgr):
            device.append(irq)

        # memory, includes expansion roms
        for devmemblock in self.getDeviceMemory(devicepath):
            device.append(devmemblock)

        # mmconf
        device.append(
            self.get_mmconf(int(getPciConfigAddress(), 16),
                            bus, devno, fn))

        # ioports
        for ioport in self.getIoports(devicepath):
            device.append(ioport)

        # reserved memory region if present
        rmrr = dmar.get_referenced_rmrr(bus, devno, fn)

        if rmrr:
            device.append(schemadata.schema.namedRefType(ref=rmrr))

        return device


class SerialDevicesCreator():

    "Helper class of DevicesCreator"

    def __init__(self):
        self.currentdev = 1

    def createElems(self, serialdevicepath):
        "Return serial devices found in given path"
        serialdevices = []
        for fname in sorted(os.listdir(serialdevicepath)):
            devpath = os.path.join(serialdevicepath, fname)
            resources = devpath + "/resources"
            if os.path.isdir(devpath) and os.path.exists(resources):
                devresources = extractor.extractData(resources)
                device = self.parseSerialDeviceResources(devresources)
                if device is not None:
                    print("  Name: " + device.name + ", Ports: " +
                          device.start + "-" + device.end +
                          (", IRQ: " + device.irq if device.irq is not None
                           else ""))
                    xmlDev = schemadata.schema.deviceType(name=device.name)

                    if device.irq is not None:
                        xmlDev.append(schemadata.schema.irqType(
                            name="irq1", number=device.irq))

                    xmlDev.append(schemadata.schema.ioPortType(
                        name="ioport1",
                        start=util.toWord64(device.start),
                        end=util.toWord64(device.end)))

                    serialdevices.append(xmlDev)

        return serialdevices

    def parseSerialDeviceResources(self, resources):
        "Parses serial device resources"
        device = None
        try:
            addrs = parseutil.parseData_Sep(resources, "io", " ")
            irq = None
            try:
                irq = parseutil.parseData_Sep(resources, "irq", " ")
            except customExceptions.KeyNotFound:
                pass
        except customExceptions.KeyNotFound:
            message.addMessage("Error parsing serial device information")
        else:
            start = addrs.partition("-")[0]
            end = addrs.partition("-")[2]
            ports = Address(start[2:], end[2:])
            name = ""
            if ports in WELL_KNOWN_IOPORTS:
                name = WELL_KNOWN_IOPORTS[ports]
            else:
                name = "serial_port_%d" % self.currentdev
                self.currentdev = self.currentdev + 1

            device = SerialDevice(name, start, end, irq)

        return device


class IoapicDevicesCreator():

    def _extract_ioapic_info(self, dmesgpath):
        """
        Extract I/O APIC maximum redirection entry count information from
        dmesg.
        """
        info = {}
        ioapic_info = re.compile(r'IOAPIC\[\d\]:\s.*(?!address)\s'
                                 r'(0x[0-9a-fA-F]*),\sGSI\s([0-9]*)-([0-9]*)')

        try:
            data = extractor.extractData(dmesgpath)
        except IOError:
            errstr = ("Could not read file: %s\n" % dmesgpath +
                      "Unable to extract I/O APIC info.")
            message.addError(errstr)

        for entry in ioapic_info.finditer(data):
            max_redir = int(entry.group(3)) - int(entry.group(2))
            info[int(entry.group(1), 0)] = max_redir

        return info

    def createElems(self, dmesg):
        elemlist = []
        ioapic_info = self._extract_ioapic_info(dmesg)

        # Create I/O APIC devices
        for i, ioapic in enumerate(madt.get_ioapics(), 1):
            elemlist.append(
                self.createDeviceFromAddr(ioapic,
                                          "ioapic_" + str(i),
                                          ioapic_info))
        return elemlist

    def createDeviceFromAddr(self, ioapic, name, ioapic_info):
        """
        Generates a device element from a given I/O APIC.
        """
        device = schemadata.schema.deviceType(name=name)

        device.append(schemadata.schema.deviceMemoryType(
            name="mem1",
            caching="UC",
            physicalAddress=util.toWord64(hex(ioapic.address)),
            size=util.toWord64(PAGE_SIZE)))

        capabilities = schemadata.schema.capabilitiesType()
        capabilities.append(schemadata.schema.capabilityType(name="ioapic"))

        # GSI base
        capabilities.append(schemadata.schema.capabilityType(
                            str(ioapic.gsi_base), name="gsi_base"))

        ioapic_id = 1
        max_redir = 23
        if ioapic.address in ioapic_info:
            max_redir = ioapic_info[ioapic.address]
            ioapic_id = ioapic.id
        else:
            message.addWarning("I/O APIC information not available in dmesg," +
                               " try running the tool immediately after boot.")

        capabilities.append(schemadata.schema.capabilityType(
                            str(max_redir), name="max_redirection_entry"))

        ioapic = dmar.get_ioapic_devices()
        for ioapic in dmar.get_ioapic_devices():
            if ioapic.id == ioapic_id:
                sid_nr = ioapic.bus << 8
                sid_nr += ioapic.device << 3
                sid_nr += ioapic.function
                sid = util.toWord64(hex(sid_nr))
                break
        else:
            message.addWarning("No I/O APIC device information for ID " +
                               str(ioapic_id) + " present in DMAR. Using " +
                               "default SID " + DEFAULT_IOAPIC_SID)
            sid = DEFAULT_IOAPIC_SID

        capabilities.append(schemadata.schema.capabilityType(
                            sid, name="source_id"))

        device.append(capabilities)

        return device


class IommuDevicesCreator():

    def createElems(self, devmempath):
        elemlist = []

        # Create Iommu devices
        for i, addr in enumerate(dmar.get_iommu_addrs(), 1):
            elemlist.append(
                self.createDeviceFromAddr(devmempath,
                                          addr,
                                          "iommu_" + str(i)))
        return elemlist

    def getIommuRegisterValue(self,
                              address,
                              devmem,
                              regsize):
        "Return IOMMU register value at given address and specified size"
        try:
            return int(extractor.extractBinaryData(devmem,
                                                   address,
                                                   regsize),
                       16)
        except IOError:
            message.addError("Could not access file: %s - %s" %
                             (devmem, hex(address)), False)
            return -1

    def getIommuAGAW(self,
                     iommuaddr,
                     devmem,
                     capoffset,
                     capbytesize,
                     agawbitstart):
        "Gets the AGAW name from a given iommuaddr, at the capability offset"
        capreg = self.getIommuRegisterValue(iommuaddr + capoffset,
                                            devmem,
                                            capbytesize)

        if capreg == -1:
            return "agaw"

        AGAW_39_BITNO = 1
        AGAW_48_BITNO = 2
        AGAW_39_NAME = "39"
        AGAW_48_NAME = "48"

        try:
            agaw = (capreg >> agawbitstart) & 0x1F  # See 5 bits
            if util.getBit(agaw, AGAW_39_BITNO):
                return AGAW_39_NAME
            elif util.getBit(agaw, AGAW_48_BITNO):
                return AGAW_48_NAME
        except Exception:
            message.addError("AGAW Capability could not be found for "
                             "IOMMU device at: %s" % iommuaddr, False)
        return "agaw"

    def createDeviceFromAddr(self,
                             devmem,
                             iommuaddr,
                             iommuname):
        "Generates a device element from a given iommu address"

        CAP_REG_OFFSET = 0x08
        CAP_REG_BYTE_SIZE = 8

        EXTCAP_REG_OFFSET = 0x10
        EXTCAP_REG_BYTE_SIZE = 8

        AGAW_BIT_START = 8

        IOMMU_SIZE = "0x1000"

        device = schemadata.schema.deviceType(name=iommuname)

        device.append(schemadata.schema.deviceMemoryType(
            name="mmio",
            caching="UC",
            physicalAddress=util.toWord64(iommuaddr),
            size=util.toWord64(IOMMU_SIZE)))

        capabilities = schemadata.schema.capabilitiesType()
        capabilities.append(schemadata.schema.capabilityType(name="iommu"))

        # agaw
        capabilities.append(schemadata.schema.capabilityType(
            self.getIommuAGAW(int(iommuaddr, 16),
                              devmem,
                              CAP_REG_OFFSET,
                              CAP_REG_BYTE_SIZE,
                              AGAW_BIT_START), name="agaw"))

        # fro, see Intel VT-d spec sections 10.4.2 and 10.4.14
        capabilities.append(schemadata.schema.capabilityType(
            str(((self.getIommuRegisterValue(
                int(iommuaddr, 16) + CAP_REG_OFFSET,
                devmem,
                CAP_REG_BYTE_SIZE) >> 24) & 0x3ff) * 16),
            name="fr_offset"))

        # iro, see Intel VT-d spec sections 10.4.3 and 10.4.8.1
        capabilities.append(schemadata.schema.capabilityType(
            str(((self.getIommuRegisterValue(
                int(iommuaddr, 16) + EXTCAP_REG_OFFSET,
                devmem,
                EXTCAP_REG_BYTE_SIZE) >> 8) & 0x3ff) * 16 + 8),
            name="iotlb_invalidate_offset"))

        device.append(capabilities)

        return device


class SystemboardDeviceCreator():

    def create_element(self):
        dev = schemadata.schema.deviceType(name="system_board")

        dev.append(schemadata.schema.ioPortType(
            name="reset",
            start=util.toWord64(hex(0xcf9)),
            end=util.toWord64(hex(0xcf9))))

        shutdown_port = fadt.get_shutdown_port()
        dev.append(schemadata.schema.ioPortType(
            name="pm1a_cnt",
            start=util.toWord64(shutdown_port),
            end=util.toWord64(shutdown_port)))

        caps = schemadata.schema.capabilitiesType()
        caps.append(schemadata.schema.capabilityType(name="systemboard"))
        # First byte in ACPI DSDT _S5 object
        caps.append(schemadata.schema.capabilityType(
            7168, name="pm1a_cnt_slp_typ"))
        dev.append(caps)

        return dev


def genDmesg(temppath, name):
    util.makefolder(temppath)
    target = os.path.join(temppath, name)
    print("Generating dmesg output to: %s" % target)
    with open(target, "w") as f:
        subprocess.check_call(["dmesg", "-s", "2097152"], stdout=f)
    return target


def createElements():
    "Creates the element tree and returns top element"

    # Choose binding module to use
    schemadata.selectSchema("hardware_config")

    # Initialise dmesg
    dmesg = genDmesg(paths.TEMP, "dmesg_tmp")

    # Parse MADT
    madt.parse_table(paths.MADT, paths.TEMP)

    # Parse MCFG
    mcfg.parse_table(paths.MCFG, paths.TEMP)

    # Parse DMAR
    dmar.parse_table(paths.DMAR, paths.TEMP)

    # Parse DSDT
    dsdt.load_prt(paths.DSDT, paths.TEMP)

    # Parse FADT
    fadt.parse_table(paths.FADT, paths.TEMP)

    # Create Elements
    hardware = schemadata.schema.hardwareType()
    hardware.append(ProcessorCreator().createElem(paths.CPUINFO, paths.MSR,
                                                  dmesg))
    hardware.append(MemoryCreator().createElem(paths.MEMMAP))
    hardware.append(DevicesCreator().createElem(dmesg))

    return hardware
