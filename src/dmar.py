#   Copyright (C) 2015 Chen Chin Jieh <chinjieh@gmail.com>
#   Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
#   Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
#
#   This file is part of mugenhwcfg.
#
#   mugenhwcfg is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   mugenhwcfg is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with mugenhwcfg.  If not, see <http://www.gnu.org/licenses/>.


# Module that handles extraction of data from the DMAR ACPI table

from collections import namedtuple

import re

from . import acpi

hud_table = re.compile(r'^.*Subtable Type : 0000.*\s'
                       r'((?!(\s.*Subtable Type))(\s|.))*', re.MULTILINE)
hud_base_addr = re.compile(r'.*Register Base Address : '
                           '(?P<address>[0-9a-fA-F]+)')
rmrr_table = re.compile(r'^.*Subtable Type : 0001.*\s'
                        r'((?!(\s.*Subtable Type))(\s|.))*', re.MULTILINE)
rmrr_start = re.compile(r'.*Base Address : (?P<start>[0-9a-fA-F]+)')
rmrr_end = re.compile(r'.*End Address.+?: (?P<end>[0-9a-fA-F]+)')
pci_dev = re.compile(r'.*Enumeration ID : (?P<enum_id>[0-9a-fA-F]{2})\s+'
                     r'.*PCI Bus Number : (?P<bus>[0-9a-fA-F]{2})\s+'
                     r'.*PCI Path :[\s\[]*(?P<dev>[0-9a-fA-F]{2}),'
                     r'\s*(?P<fun>[0-9a-fA-F]{2})[\s\[]*')
ioapic_devs = re.compile(r'^.*Device Scope Type : 03.*\s'
                         r'((?!(\s.*Device Scope Type))(\s|.))*', re.MULTILINE)

Rmrr_Region = namedtuple("Rmrr_Region", "name, start, end")
PCI_Dev = namedtuple("PCI_Dev", "bus, device, function, rmrr_name")
IO_Apic = namedtuple("IO_Apic", "bus, device, function, id")

_ioapics = []
_iommu_addrs = []
_rmrrs = []
_devices = []


def _parse_hardware_unit_definitions(data):
    """
    Parse DMAR hardware unit definition subtables and store the extracted
    IOMMU register base addresses.
    """
    addrs = []
    for subtable in hud_table.finditer(data):
        match = hud_base_addr.search(subtable.group(0))
        if match:
            addrs.append("0x" + match.group("address").lower())

    return addrs


def _parse_ioapic_devices(data):
    """
    Parse I/O APIC device scopes and return the extracted PCI addresses.
    """
    devs = []
    for dev_scope in ioapic_devs.finditer(data):
        for dev in pci_dev.finditer(dev_scope.group(0)):
            devs.append(IO_Apic(bus=int(dev.group("bus"), 16),
                        device=int(dev.group("dev"), 16),
                        function=int(dev.group("fun"), 16),
                        id=int(dev.group("enum_id"), 16)))

    return devs


def _parse_rmrrs(data):
    regions, devices = [], []
    for i, rmrr in enumerate(rmrr_table.finditer(data), 1):
        start, end = 0, 0
        region_name = "rmrr" + str(i)
        match = rmrr_start.search(rmrr.group(0))
        if match:
            start = "0x" + match.group("start").lower()
        match = rmrr_end.search(rmrr.group(0))
        if match:
            end = "0x" + match.group("end").lower()

        for dev in pci_dev.finditer(rmrr.group(0)):
            devices.append(PCI_Dev(bus=int(dev.group("bus"), 16),
                           device=int(dev.group("dev"), 16),
                           function=int(dev.group("fun"), 16),
                           rmrr_name=region_name))

        regions.append(Rmrr_Region(name=region_name, start=start, end=end))

    return regions, devices


def parse_table(path, temp_path):
    """
    Parses a DMAR table
    """
    global _ioapics, _iommu_addrs, _rmrrs, _devices

    print("> Decompiling DMAR table")
    data = acpi.decompile_table(path, temp_path,
                                "Could not obtain DMAR information; "
                                "IOMMU device information not found.")

    print("> Parsing IOMMU hardware unit definitions")
    _iommu_addrs = _parse_hardware_unit_definitions(data)
    print("> Parsing IOMMU RMRRs")
    _rmrrs, _devices = _parse_rmrrs(data)
    print("> Parsing I/O APIC information")
    _ioapics = _parse_ioapic_devices(data)


def get_iommu_addrs():
    """
    Return IOMMU register base addresses extracted from DMAR table
    """
    return _iommu_addrs


def get_reserved_mem_regions():
    """
    Return reserved memory regions extracted from DMAR table
    """
    return _rmrrs


def get_referenced_rmrr(bus, device, fn):
    """
    Returns the name of the reserved memory region referenced by the specified
    PCI device. If the given device does not reference an RMRR 'none' is
    returned.
    """
    for dev in _devices:
        if dev.bus == bus and dev.device == device and dev.function == fn:
            return dev.rmrr_name

    return None


def get_ioapic_devices():
    """
    Return I/O APIC source IDs as PCI address.
    """
    return _ioapics
